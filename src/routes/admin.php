<?php

Route::group(['middleware' => ['web']], function () {

    Route::group(['prefix' => config('sam-admin.admin-route-prefix')], function() {

        Route::group(['middleware' => ['auth:'.config('sam-admin.guard')]], function () {

            Route::group(['prefix' => 'customers', 'middleware' => 'permission:customers.view|customers.create|customers.edit|customers.show|customers.delete'], function() {

                Route::get('/', 'Totem\SamCustomers\App\Controllers\CustomersController@index')->middleware('permission:customers.view')->name('customers.index');
                Route::get('create', 'Totem\SamCustomers\App\Controllers\CustomersController@create')->middleware('permission:customers.create')->name('customers.create');
                Route::post('create', 'Totem\SamCustomers\App\Controllers\CustomersController@store')->middleware('permission:customers.create')->name('customers.store');
                Route::get('{id}', 'Totem\SamCustomers\App\Controllers\CustomersController@show')->middleware('permission:customers.show')->name('customers.show')->where('id', '[0-9]+');

                Route::group(['middleware' => 'permission:customers.edit'], function() {
                    Route::get('{id}/edit', 'Totem\SamCustomers\App\Controllers\CustomersController@edit')->name('customers.edit')->where('id', '[0-9]+');
                    Route::patch('{id}', 'Totem\SamCustomers\App\Controllers\CustomersController@update')->name('customers.update')->where('id', '[0-9]+');
                    Route::patch('{id}/activation', 'Totem\SamCustomers\App\Controllers\CustomersController@activate')->name('customers.activation')->where('id', '[0-9]+');
                    Route::patch('{id}/deactivation', 'Totem\SamCustomers\App\Controllers\CustomersController@deactivate')->name('customers.deactivation')->where('id', '[0-9]+');
                    Route::patch('{id}/company', 'Totem\SamCustomers\App\Controllers\CustomersController@companyUpdate')->name('customers.update.company')->where('id', '[0-9]+');

                });

                Route::delete('{id}','Totem\SamCustomers\App\Controllers\CustomersController@destroy')->middleware('permission:customers.delete')->name('customers.destroy')->where('id', '[0-9]+');
            });

            Route::group(['prefix' => 'groups', 'middleware' => 'permission:customer-groups.view|customer-groups.modify'], function() {

                Route::group(['middleware' => 'permission:customer-groups.view'], function() {
                    Route::get('/', 'Totem\SamCustomers\App\Controllers\GroupsController@index')->name('groups.index');
                    Route::get('{id}', 'Totem\SamCustomers\App\Controllers\GroupsController@show')->name('groups.show')->where('id', '[0-9]+');
                });

                Route::group(['middleware' => 'permission:customer-groups.modify'], function() {
                    Route::get('create', 'Totem\SamCustomers\App\Controllers\GroupsController@create')->name('groups.create');
                    Route::post('create', 'Totem\SamCustomers\App\Controllers\GroupsController@store')->name('groups.store');
                    Route::get('{id}/edit', 'Totem\SamCustomers\App\Controllers\GroupsController@edit')->name('groups.edit')->where('id', '[0-9]+');
                    Route::patch('{id}', 'Totem\SamCustomers\App\Controllers\GroupsController@update')->name('groups.update')->where('id', '[0-9]+');
                    Route::delete('{id}', 'Totem\SamCustomers\App\Controllers\GroupsController@destroy')->name('groups.destroy')->where('id', '[0-9]+');
                });
            });

        });
    });

});