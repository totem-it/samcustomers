<?php

Route::group(['prefix' => 'api' ], function() {

    Route::middleware('api')->group(function() {

        Route::post('register', 'Totem\SamCustomers\App\Controllers\Api\ApiRegisterController@register')->name('api.register');
        Route::post('register/company', 'Totem\SamCustomers\App\Controllers\Api\ApiRegisterController@registerWithCompany')->name('api.register-company');

        Route::post('login', 'Totem\SamCustomers\App\Controllers\Api\ApiLoginController@login')->name('api.login');
        Route::post('refresh', 'Totem\SamCustomers\App\Controllers\Api\ApiLoginController@refresh')->name('api.refresh');
        Route::post('logout', 'Totem\SamCustomers\App\Controllers\Api\ApiLoginController@logout')->name('api.logout');
    });

    Route::group(['prefix' => 'profile'], function() {

        Route::get('/', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@profile')->name('api.profile');

    });


});