<?php

Route::group(['middleware' => ['web']], function () {


    Route::get('login', 'Totem\SamCustomers\App\Controllers\Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Totem\SamCustomers\App\Controllers\Auth\LoginController@login')->name('login.submit');

    Route::group(['prefix' => 'register'], function() {
        Route::get('/', 'Totem\SamCustomers\App\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('/', 'Totem\SamCustomers\App\Controllers\Auth\RegisterController@register')->name('register.submit');
        Route::get('company', 'Totem\SamCustomers\App\Controllers\Auth\RegisterController@showCompanyRegistrationForm')->name('register.company');
    });

    Route::get('logout', 'Totem\SamCustomers\App\Controllers\Auth\LoginController@logout')->name('logout');

    /*
     * Nie zaimplementowane RESETOWANIE
     */
//    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//    Route::post('password/reset', 'Auth\ResetPasswordController@reset');


    Route::group(['prefix' => 'profile', 'middleware' => 'auth:customer'], function() {

        Route::get('/', 'Totem\SamCustomers\App\Controllers\AccountController@profile')->name('account.profile');

        Route::group(['prefix' => 'settings'], function() {
            Route::get('/', 'Totem\SamCustomers\App\Controllers\AccountController@settings')->name('account.settings');
            Route::patch('/', 'Totem\SamCustomers\App\Controllers\AccountController@update')->name('account.settings.update');
            Route::patch('password', 'Totem\SamCustomers\App\Controllers\AccountController@passwordUpdate')->name('account.settings.update.password');
            Route::patch('company', 'Totem\SamCustomers\App\Controllers\AccountController@companyUpdate')->name('account.settings.update.company');

            Route::group(['prefix' => 'address-book'], function() {
                Route::get('/', 'Totem\SamCustomers\App\Controllers\AddressController@index')->name('account.settings.address-book');
                Route::get('{id}/edit', 'Totem\SamCustomers\App\Controllers\AddressController@edit')->name('account.settings.address-book.edit')->where('id', '[0-9]+');
                Route::patch('{id}', 'Totem\SamCustomers\App\Controllers\AddressController@update')->name('account.settings.address-book.update')->where('id', '[0-9]+');
                Route::post('create', 'Totem\SamCustomers\App\Controllers\AddressController@store')->name('account.settings.address-book.store');
                Route::delete('{id}', 'Totem\SamCustomers\App\Controllers\AddressController@destroy')->name('account.settings.address-book.destroy')->where('id', '[0-9]+');
                Route::get('{id}', 'Totem\SamCustomers\App\Controllers\AddressController@show')->where('id', '[0-9]+');
            });
        });
    });

});