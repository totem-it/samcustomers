<?php

Route::group(['prefix' => 'api' ], function() {

    Route::middleware(config('sam-admin.guard-api'))->group(function() {

        Route::group(['prefix' => 'customers'], function() {

            Route::get('/', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@index')->middleware('permission:customers.view');
            Route::post('/', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@create')->middleware('permission:customers.create');
            Route::delete('{id}','Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@destroy')->middleware('permission:customers.delete')->where('id', '[0-9]+');

            Route::group(['middleware' => 'permission:customers.edit'], function() {
                Route::put('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@replace')->where('id', '[0-9]+');
                Route::patch('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@update')->where('id', '[0-9]+');

            });

            Route::group(['middleware' => 'permission:customers.show'], function() {
                Route::get('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@show')->where('id', '[0-9]+');
                Route::get('{id}/all', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@showAll')->where('id', '[0-9]+');
                Route::get('{id}/company', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@showCompany')->where('id', '[0-9]+');
                Route::get('{id}/groups', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@showGroups')->where('id', '[0-9]+');
                Route::get('{id}/addresses', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@showAddresses')->where('id', '[0-9]+');
                Route::get('{id}/addresses/billing', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@showBillingAddresses')->where('id', '[0-9]+');
                Route::get('{id}/addresses/shipping', 'Totem\SamCustomers\App\Controllers\Api\ApiCustomersController@showShippingAddresses')->where('id', '[0-9]+');
            });

            Route::group(['prefix' => 'groups'], function() {

                Route::group(['middleware' => 'permission:customer-groups.view'], function() {
                    Route::get('/', 'Totem\SamCustomers\App\Controllers\Api\ApiGroupsController@index');
                    Route::get('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiGroupsController@show')->where('id', '[0-9]+');
                });

                Route::group(['middleware' => 'permission:customer-groups.modify'], function() {
                    Route::post('/', 'Totem\SamCustomers\App\Controllers\Api\ApiGroupsController@create');
                    Route::put('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiGroupsController@replace')->where('id', '[0-9]+');
                    Route::delete('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiGroupsController@destroy')->where('id', '[0-9]+');
                });

            });

            Route::group(['prefix' => 'companies'], function() {

                Route::get('/', 'Totem\SamCustomers\App\Controllers\Api\ApiCompanyController@index')->middleware('permission:customers.view');
                Route::get('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiCompanyController@show')->middleware('permission:customers.show')->where('id', '[0-9]+');

                Route::group(['middleware' => 'permission:customers.edit'], function() {
                    Route::post('/', 'Totem\SamCustomers\App\Controllers\Api\ApiCompanyController@create');
                    Route::put('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiCompanyController@replace')->where('id', '[0-9]+');
                    Route::delete('{id}', 'Totem\SamCustomers\App\Controllers\Api\ApiCompanyController@destroy')->where('id', '[0-9]+');
                });
            });

        });

    });

});