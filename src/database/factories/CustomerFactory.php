<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

$factory->define(\Totem\SamCustomers\App\Model\Customer::class, function (Faker $faker) {
    return [
        'email'             => $faker->unique()->safeEmail,
        'password'          => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'firstname'         => $faker->firstName,
        'lastname'          => $faker->lastName,
        'gender'            => $faker->randomElement(\Totem\SamCustomers\App\Enums\GenderType::getValues()),
        'phone_number'      => $faker->phoneNumber,
        'company_id'        => $faker->optional(0.3)->numberBetween(1, 50),
        'remember_token'    => str_random(10),
        'active'            => $faker->numberBetween(0, 1),
    ];
});
$factory->state(\Totem\SamCustomers\App\Model\Customer::class, 'no-company', [
    'company_id' => null,
]);