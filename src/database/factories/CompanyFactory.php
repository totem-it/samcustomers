<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

$factory->define(\Totem\SamCustomers\App\Model\Company::class, function (Faker $faker) {
    return [
        'nip'           => $faker->unique()->taxpayerIdentificationNumber,
        'name'          => $faker->unique()->company,
        'street'        => $faker->streetName,
        'street_number' => $faker->buildingNumber,
        'place_number'  => $faker->optional()->randomDigit,
        'post_code'     => $faker->postcode,
        'city'          => $faker->city,
        'country_code'  => $faker->optional(0.3, 'pl')->languageCode,
        'phone_number'  => $faker->unique()->phoneNumber,
        'email'         => $faker->unique()->companyEmail,
        'regon'         => $faker->regon,
    ];
});
