<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

$factory->define(\Totem\SamCustomers\App\Model\Group::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->userName,
        'name' => $faker->unique()->domainWord,
    ];
});
