<?php

namespace Totem\SamCustomers\Database\Seeds;

use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        if (config('app.env') !== 'production') {
            factory(\Totem\SamCustomers\App\Model\Company::class, 55)->create();
        }
    }
}
