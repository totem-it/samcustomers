<?php

namespace Totem\SamCustomers\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamAcl\Database\PermissionTraitSeeder;

class PermissionSeeder extends Seeder
{

    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'customers.view',
                'name' => 'Can View Customers',
                'description' => 'Can view customers',
            ],
            [
                'slug' => 'customers.create',
                'name' => 'Can Create Customers',
                'description' => 'Can create new customers',
            ],
            [
                'slug' => 'customers.edit',
                'name' => 'Can Edit Customers',
                'description' => 'Can edit customers',
            ],
            [
                'slug' => 'customers.show',
                'name' => 'Can Show Customers',
                'description' => 'Can show customers',
            ],
            [
                'slug' => 'customers.delete',
                'name' => 'Can Delete Customers',
                'description' => 'Can delete customers',
            ],
            [
                'slug' => 'customer-groups.view',
                'name' => 'Can View Customer Groups',
                'description' => 'Can view customer groups',
            ],
            [
                'slug' => 'customer-groups.modify',
                'name' => 'Can Modify Customer Groups',
                'description' => 'Can modify customer groups',
            ],
        ];
    }

}
