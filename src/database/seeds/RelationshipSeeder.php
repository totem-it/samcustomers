<?php

namespace Totem\SamCustomers\Database\Seeds;

use Illuminate\Database\Seeder;

class RelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $groups = \Totem\SamCustomers\App\Model\Group::all();
        $customers = \Totem\SamCustomers\App\Model\Customer::all();

        if ($customers->isNotEmpty()) {
            foreach ($customers as $customer) {
                /**
                 * @var \Totem\SamCustomers\App\Model\Customer $customer
                 */
                $customer->groups()->attach($groups->random());
            }
        }
    }
}
