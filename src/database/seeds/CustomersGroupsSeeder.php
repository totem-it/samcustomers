<?php

namespace Totem\SamCustomers\Database\Seeds;

use Illuminate\Database\Seeder;

class CustomersGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $now = now();

        \Totem\SamCustomers\App\Model\Group::insert([
            [
                'created_at' => $now,
                'updated_at' => $now,
                'code' => 'retail',
                'name' => 'Retail'
            ],
            [
                'created_at' => $now,
                'updated_at' => $now,
                'code' => 'wholesale',
                'name' => 'Wholesale'
            ],
        ]);
    }
}
