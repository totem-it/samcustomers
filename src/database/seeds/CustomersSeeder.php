<?php

namespace Totem\SamCustomers\Database\Seeds;

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        if (config('app.env') !== 'production') {
            factory(\Totem\SamCustomers\App\Model\Customer::class, 125)->create();
        }
    }
}
