<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('customers', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->string('email')->unique();
                $table->string('password');
                $table->string('firstname')->nullable();
                $table->string('lastname')->nullable();
                $table->string('gender', 1)->default('u');
                $table->string('phone_number')->nullable();
                $table->integer('company_id')->unsigned()->nullable();
                $table->dateTime('last_login')->nullable();
                $table->rememberToken();
                $table->tinyInteger('active')->default(1);
                $table->softDeletes();

                $table->foreign('company_id')->references('id')->on('companies')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamCustomers\Database\Seeds\CustomersSeeder::class
        ]);

        Artisan::call('db:seed', [
            '--class' => \Totem\SamCustomers\Database\Seeds\PermissionSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        Schema::dropIfExists('customers');

        (new \Totem\SamCustomers\Database\Seeds\PermissionSeeder)->down();
    }
}
