<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        try{
            Schema::create('companies', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->string('nip')->unique();
                $table->string('name');
                $table->string('street');
                $table->string('street_number');
                $table->string('place_number')->nullable();
                $table->string('post_code');
                $table->string('city');
                $table->string('country_code')->default('pl');
                $table->string('phone_number')->nullable();
                $table->string('email')->nullable();
                $table->string('regon')->nullable();
                $table->softDeletes();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamCustomers\Database\Seeds\CompaniesSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists('companies');
    }
}
