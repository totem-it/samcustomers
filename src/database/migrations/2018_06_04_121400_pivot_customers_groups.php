<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotCustomersGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        try{
            Schema::create('customer_group', function (Blueprint $table) {
                $table->integer('customer_id')->unsigned();
                $table->integer('group_id')->unsigned();

                $table->foreign('customer_id')->references('id')->on('customers')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('group_id')->references('id')->on('customers_groups')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['customer_id', 'group_id']);
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamCustomers\Database\Seeds\RelationshipSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists('customer_group');
    }
}
