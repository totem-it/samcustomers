<?php

namespace Totem\SamCustomers\Tests;

use Totem\SamCustomers\App\Model\Customer;

trait AttachCustomerJwtToken
{

    protected function setUser() : \Tymon\JWTAuth\Contracts\JWTSubject
    {
        /** @var Customer $user */
        $user =  $this->loginUser ?: factory(Customer::class)->state('no-company')->create();
        $this->actingAs($user, $user->getJWTCustomClaims()['gd']);

        return $user;
    }

}