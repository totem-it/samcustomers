<?php

namespace Totem\SamCustomers\App\Requests;

use Totem\SamCustomers\App\Enums\GenderType;
use Totem\SamAdmin\App\Requests\BaseRequest;

class CustomerRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'firstname'     => 'required',
            'lastname'      => 'required',
            'email'         => 'required|email|unique:customers,email,' . $this->id,
            'password'      => $this->method() === 'POST' ? 'required' : '',
            'phone_number'  => 'required',
            'gender'        => 'required|enum_value:'.GenderType::class,
        ];
    }

    public function attributes() : array
    {
        return [
            'firstname'     => __('First name'),
            'lastname'      => __('Last name'),
            'email'         => __('Email Address'),
            'phone_number'  => __('Phone number'),
            'gender'        => __('Gender'),
        ];
    }
}
