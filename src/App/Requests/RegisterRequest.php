<?php

namespace Totem\SamCustomers\App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    public function authorize() : bool
    {
        return true;
    }

    public function rules() : array
    {
        return [
            'email' => 'required|string|email|max:255|unique:customers',
            'password' => 'required|string|min:6',
            'agreementTerms' => 'accepted',
            'agreementProcessing' => 'accepted',

            'nip'           => 'string',
            'name'          => 'required_with:nip',
            'country'       => 'required_with:nip',
            'street'        => 'required_with:nip',
            'street_number' => 'required_with:nip',
            'post_code'     => 'required_with:nip',
            'city'          => 'required_with:nip',
            'phone_number'  => 'required_with:nip',
            'regon'         => 'required_with:nip',
        ];
    }

    public function messages() : array
    {
        return [
            'agreementTerms.accepted'       => __('You must accept the terms and conditions.'),
            'agreementProcessing.accepted'  => __('You must agree to the processing of data.'),
            'name.required_with'            => __('The :attribute field is required.'),
            'country.required_with'         => __('The :attribute field is required.'),
            'street.required_with'          => __('The :attribute field is required.'),
            'street_number.required_with'   => __('The :attribute field is required.'),
            'post_code.required_with'       => __('The :attribute field is required.'),
            'city.required_with'            => __('The :attribute field is required.'),
            'phone_number.required_with'    => __('The :attribute field is required.'),
            'regon.required_with'           => __('The :attribute field is required.'),
        ];
    }

    public function attributes() : array
    {
        return [
            'nip'           => __('NIP'),
            'name'          => __('Name'),
            'firstname'     => __('First name'),
            'lastname'      => __('Last name'),
            'country'       => __('Country'),
            'street'        => __('Street'),
            'street_number' => __('Street number'),
            'post_code'     => __('Post code'),
            'city'          => __('City'),
            'phone_number'  => __('Phone number'),
            'regon'         => __('Regon'),
        ];
    }
}
