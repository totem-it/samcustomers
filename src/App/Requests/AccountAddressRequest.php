<?php

namespace Totem\SamCustomers\App\Requests;

use Totem\SamAddress\App\Model\Address;
use Totem\SamAddress\App\Requests\AddressRequest;

class AccountAddressRequest extends AddressRequest
{

    public function rules() : array
    {
        return [
            'firstname'         => 'required',
            'lastname'          => 'required',
            'street'            => 'required',
            'street_number'     => 'required',
            'post_code'         => 'required',
            'city'              => 'required',
            'country_code'      => 'required',
            'phone_number'      => 'required',
            'email'             => 'required|email',
        ];
    }

    protected function getValidatorInstance() : \Illuminate\Contracts\Validation\Validator
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function () {
            $this->merge([
                'label' => "{$this->get('firstname')} {$this->get('lastname')}",
                Address::model_id => auth()->id()
            ]);
        });

        return $validator;
    }
}
