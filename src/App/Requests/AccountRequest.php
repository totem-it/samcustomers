<?php

namespace Totem\SamCustomers\App\Requests;

use Totem\SamAdmin\App\Requests\BaseRequest;

class AccountRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'email-account'         => 'required|email|unique:customers,email,' . $this->user()->id,
            'phone_number-account'  => 'required',
            'gender'                => 'required|in:u,m,f',
        ];
    }

    public function attributes() : array
    {
        return [
            'firstname'             => __('First name'),
            'lastname'              => __('Last name'),
            'email-account'         => __('Email Address'),
            'phone_number-account'  => __('Phone number'),
            'gender'                => __('Gender'),
        ];
    }

}
