<?php

namespace Totem\SamCustomers\App\Requests;

use Totem\SamAdmin\App\Requests\BaseRequest;

class GroupRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'code' => ($this->method() === 'POST' ? 'required|' : '') . 'unique:customers_groups,code,' . $this->id,
            'name' => 'required|unique:customers_groups,name,' . $this->id,
        ];
    }

    public function attributes() : array
    {
        return [
            'code' => __('Code'),
            'name' => __('Name'),
        ];
    }
}
