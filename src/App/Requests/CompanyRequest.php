<?php

namespace Totem\SamCustomers\App\Requests;

use Totem\SamAdmin\App\Requests\BaseRequest;

class CompanyRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'nip'           => 'required',
            'name'          => 'required',
            'street'        => 'required',
            'street_number' => 'required',
            'post_code'     => 'required',
            'city'          => 'required',
            'country_code'  => 'required',
            'phone_number'  => 'required',
            'email'         => 'required|email',
        ];
    }

    public function attributes() : array
    {
        return [
            'nip'           => __('Nip'),
            'name'          => __('Name'),
            'street'        => __('Street'),
            'street_number' => __('Street number'),
            'post_code'     => __('Post code'),
            'city'          => __('City'),
            'country_code'  => __('Country'),
            'phone_number'  => __('Phone number'),
            'email'         => __('Email'),
            'regon'         => __('Regon'),
        ];
    }

}
