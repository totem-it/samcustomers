<?php

namespace Totem\SamCustomers\App\Requests\Api;

class CompanyRegisterRequest extends CustomerRegisterRequest
{

    public function rules() : array
    {
        return array_merge([
            'nip'           => 'required',
            'name'          => 'required',
            'country'       => 'required',
            'street'        => 'required',
            'street_number' => 'required',
            'post_code'     => 'required',
            'city'          => 'required',
            'phone_number'  => 'required',
            'regon'         => 'required',
        ], parent::rules());
    }

}
