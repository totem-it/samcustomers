<?php

namespace Totem\SamCustomers\App\Requests\Api;

use Totem\SamCustomers\App\Enums\GenderType;
use Totem\SamCustomers\App\Requests\RegisterRequest;

class CustomerRegisterRequest extends RegisterRequest
{

    public function rules() : array
    {
        return [
            'email'         => 'required|string|email|max:255|unique:customers',
            'password'      => 'required|string|min:6',
            'firstname'     => 'required',
            'lastname'      => 'required',
            'phone_number'  => 'string',
            'gender'        => 'enum_value:'.GenderType::class,
        ];
    }

}
