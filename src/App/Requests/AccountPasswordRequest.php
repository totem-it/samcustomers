<?php

namespace Totem\SamCustomers\App\Requests;

use Totem\SamAdmin\App\Requests\BaseRequest;

class AccountPasswordRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'current-password'      => 'required|current_password',
            'password'              => 'required|min:6',
            'confirm-password'      => 'same:password',
        ];
    }

    public function attributes() : array
    {
        return [
            'password'      => __('Password'),
            'confirm-password'      => __('Repeat password'),
        ];
    }

    public function messages() : array
    {
        return [
            'current_password'     => __('Provided password is different than the current one.'),
        ];
    }
}
