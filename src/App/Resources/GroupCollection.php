<?php

namespace Totem\SamCustomers\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class GroupCollection extends ApiCollection
{

    public $collects = \Totem\SamCustomers\App\Resources\GroupResource::class;

}
