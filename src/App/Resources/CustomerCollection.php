<?php

namespace Totem\SamCustomers\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class CustomerCollection extends ApiCollection
{

    public $collects = \Totem\SamCustomers\App\Resources\CustomerResource::class;

}
