<?php

namespace Totem\SamCustomers\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

class CompanyResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'            => $this->id,
            'nip'           => $this->nip,
            'name'          => $this->name,
            'street'        => $this->street,
            'street_number' => $this->street_number,
            'place_number'  => $this->place_number,
            'post_code'     => $this->post_code,
            'city'          => $this->city,
            'country_code'  => $this->country_code,
            'phone_number'  => $this->phone_number,
            'email'         => $this->email,
            'regon'         => $this->regon,
            'customer'      => $this->when(
                $request->user()->can('customers.show'),
                CustomerResource::collection($this->whenLoaded('customer'))
            ),
        ];
    }

}
