<?php

namespace Totem\SamCustomers\App\Resources;

use Totem\SamAddress\App\Resources\AddressResource;
use Totem\SamAddress\App\Resources\ShippingAddressCollection;
use Totem\SamCore\App\Resources\ApiResource;

class CustomerResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'                => $this->id,
            'email'             => $this->email,
            'firstname'         => $this->firstname,
            'lastname'          => $this->lastname,
            'gender'            => $this->gender,
            'phone_number'      => $this->phone_number,
            'active'            => $this->active,
            'groups'            => GroupResource::collection($this->whenLoaded('groups')),
            'company'           => new CompanyResource($this->whenLoaded('company')),
            'addresses'         => AddressResource::collection($this->whenLoaded('addresses')),
            'billingAddress'    => new AddressResource($this->whenLoaded('billingAddress')),
            'shippingAddress'   => new ShippingAddressCollection($this->whenLoaded('shippingAddress')),
        ];
    }

}
