<?php

namespace Totem\SamCustomers\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

class GroupResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'        => $this->id,
            'code'      => $this->code,
            'name'      => $this->name,
            'customers' => $this->when(
                $request->user()->can('customers.show'),
                CustomerResource::collection($this->whenLoaded('customers'))
            ),
        ];
    }

}
