<?php

namespace Totem\SamCustomers\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class CompanyCollection extends ApiCollection
{

    public $collects = \Totem\SamCustomers\App\Resources\CompanyResource::class;

}
