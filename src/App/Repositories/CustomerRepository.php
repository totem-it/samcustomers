<?php

namespace Totem\SamCustomers\App\Repositories;

use Totem\SamCustomers\App\Model\Customer;
use Totem\SamCustomers\App\Enums\GenderType;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCustomers\App\Repositories\Contracts\CustomerRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\Totem\SamCustomers\App\Model\Customer model
 */
class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{

    public function model(): string
    {
        return Customer::class;
    }

    public function getGenderList() : array
    {
        return GenderType::toSelectArray();
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Customer
    {
        if ($id === 0) {
            throw new RepositoryException( __('No customer id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or customer not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findById(int $id = 0, array $columns = ['*']) : Customer
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Customer
    {
        $customer = ($id === 0) ? $this->model : $this->find($id);

        $customer->firstname    = $request->input('firstname');
        $customer->lastname     = $request->input('lastname');
        $customer->email        = $request->input('email');
        $customer->gender       = $request->input('gender', GenderType::UNKNOWN);
        $customer->phone_number = $request->input('phone_number');

        if (!empty($request->input('password'))) {
            $customer->password = bcrypt($request->input('password'));
        }

        if ($id > 0) {
            $customer->syncGroups([]);
        }

        $customer->save();
        $customer->attachGroups($request->input('groups', []));

        if (!empty($request->input('company_id'))) {
            $customer->attachCompany($request->input('company_id'));
        }

        return $customer;
    }

    public function storeInfo(\Illuminate\Http\Request $request, int $id = 0) : void
    {
        $this->update([
            'firstname'     => $request->input('firstname'),
            'lastname'      => $request->input('lastname'),
            'email'         => $request->input('email-account'),
            'phone_number'  => $request->input('phone_number-account'),
            'gender'        => $request->input('gender', GenderType::getValue(GenderType::UNKNOWN)),
        ], $id);
    }

    public function storePassword(\Illuminate\Http\Request $request, int $id = 0) : void
    {
        $this->update([
            'password' => bcrypt($request->input('password')),
        ], $id);
    }

//    public function findWithRelationByCustomer(int $customerId, int $id = 0, array $relationships = [], array $columns = ['*']) : Customer
//    {
//        $data = $this->findWithRelationsById($id, $relationships, $columns);
//
//        if ($data->addressable_id !== $customerId) {
//            throw new RepositoryException(  __('Given customer id :code is not correct.', ['code' => $id]), 404);
//        }
//
//        return $data;
//    }

    public function findCustomerCompany(int $id = 0, array $columns = ['*']) : \Totem\SamCustomers\App\Model\Company
    {
        $data = $this->findWithRelationsById($id, ['company'], $columns);

        if ($data->company_id === null || $data->hasCompany() === false ) {
            throw new RepositoryException(  __('Given customer id :code does not have a company.', ['code' => $id]), 404);
        }

        return $data->company;
    }

    public function findCustomerGroups(int $id = 0, array $columns = ['*']) : \Illuminate\Support\Collection
    {
        $data = $this->findWithRelationsById($id, ['groups'], $columns);

        if ($data->groups->isEmpty()) {
            throw new RepositoryException(  __('Given customer id :code does not have a groups.', ['code' => $id]), 404);
        }

        return $data->groups;
    }

    public function findCustomerAddresses(int $id = 0, array $columns = ['*']) : \Illuminate\Support\Collection
    {
        $data = $this->findWithRelationsById($id, ['addresses'], $columns);

        if ($data->addresses->isEmpty()) {
            throw new RepositoryException(  __('Given customer id :code does not have an addresses.', ['code' => $id]), 404);
        }

        return $data->addresses;
    }

    public function findCustomerBillingAddress(int $id = 0, array $columns = ['*']) : \Totem\SamAddress\App\Model\BillingAddress
    {
        $data = $this->findWithRelationsById($id, ['billingAddress'], $columns);

        if ($data->has_billing_address === false) {
            throw new RepositoryException(  __('Given customer id :code does not have an addresses.', ['code' => $id]), 404);
        }

        return $data->billingAddress;
    }

    public function findCustomerShippingAddress(int $id = 0, array $columns = ['*']) : \Illuminate\Support\Collection
    {
        $data = $this->findWithRelationsById($id, ['shippingAddress'], $columns);

        if ($data->has_shipping_address === false) {
            throw new RepositoryException(  __('Given customer id :code does not have an addresses.', ['code' => $id]), 404);
        }

        return $data->shippingAddress;
    }

}