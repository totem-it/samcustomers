<?php

namespace Totem\SamCustomers\App\Repositories;

use Totem\SamCustomers\App\Model\Group;
use Totem\SamCustomers\App\Repositories\Contracts\GroupRepositoryInterface;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Exceptions\RepositoryException;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\Totem\SamCustomers\App\Model\Group model
 */
class GroupRepository extends BaseRepository implements GroupRepositoryInterface
{

    public function model(): string
    {
        return Group::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Group
    {
        if ($id === 0) {
            throw new RepositoryException( __('No group id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or group not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findById(int $id = 0, array $columns = ['*']) : Group
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Group
    {
        $group = ($id === 0) ? $this->model : $this->find($id);

        if ($id === 0) {
            $group->code     = $request->input('code');
        }
        $group->name         = $request->input('name');
        $group->save();

        return $group;
    }

}