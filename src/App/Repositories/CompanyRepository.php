<?php

namespace Totem\SamCustomers\App\Repositories;

use Totem\SamCustomers\App\Model\Company;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCustomers\App\Repositories\Contracts\CompanyRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\Totem\SamCustomers\App\Model\Company model
 */
class CompanyRepository extends BaseRepository implements CompanyRepositoryInterface
{

    public function model(): string
    {
        return Company::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Company
    {
        if ($id === 0) {
            throw new RepositoryException( __('No company id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or company not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findById(int $id = 0, array $columns = ['*']) : Company
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Company
    {
        $company = ($id === 0) ? $this->model : $this->find($id);

        $company->nip              = $request->input('nip');
        $company->name             = $request->input('name');
        $company->street           = $request->input('street');
        $company->street_number    = $request->input('street_number');
        $company->place_number     = $request->input('place_number', null);
        $company->post_code        = $request->input('post_code');
        $company->city             = $request->input('city');
        $company->country_code     = $request->input('country_code');
        $company->phone_number     = $request->input('phone_number');
        $company->email            = $request->input('email');
        $company->regon            = $request->input('regon', null);

        $company->save();

        return $company;
    }

}