<?php

namespace Totem\SamCustomers\App\Repositories\Contracts;

use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

/**
 * @method  \Totem\SamCustomers\App\Model\Customer activate(int $id, bool $toActivate = true)
 * @method  \Totem\SamCustomers\App\Model\Customer deactivate(int $id)
 */
interface CustomerRepositoryInterface extends RepositoryInterface
{

    public function getGenderList() : array;

    public function findById(int $id = 0, array $columns = ['*']) : \Totem\SamCustomers\App\Model\Customer;

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : \Totem\SamCustomers\App\Model\Customer;

    public function findCustomerCompany(int $id = 0, array $columns = ['*']) : \Totem\SamCustomers\App\Model\Company;

    public function findCustomerGroups(int $id = 0, array $columns = ['*']) : \Illuminate\Support\Collection;

    public function findCustomerAddresses(int $id = 0, array $columns = ['*']) : \Illuminate\Support\Collection;

    public function findCustomerBillingAddress(int $id = 0, array $columns = ['*']) : \Totem\SamAddress\App\Model\BillingAddress;

    public function findCustomerShippingAddress(int $id = 0, array $columns = ['*']) : \Illuminate\Support\Collection;

    public function store(\Illuminate\Http\Request $request, int $id = 0) : \Totem\SamCustomers\App\Model\Customer;

    public function storeInfo(\Illuminate\Http\Request $request, int $id = 0) : void;

    public function storePassword(\Illuminate\Http\Request $request, int $id = 0) : void;

}