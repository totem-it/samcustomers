<?php

namespace Totem\SamCustomers\App\Repositories\Contracts;

use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;
use Totem\SamCustomers\App\Model\Company;

interface CompanyRepositoryInterface extends RepositoryInterface
{

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Company;

    public function findById(int $id = 0, array $columns = ['*']) : Company;

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Company;
}