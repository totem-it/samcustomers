<?php

namespace Totem\SamCustomers\App\Repositories\Contracts;

use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface GroupRepositoryInterface extends RepositoryInterface
{

    public function findById(int $id = 0, array $columns = ['*']) : \Totem\SamCustomers\App\Model\Group;

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : \Totem\SamCustomers\App\Model\Group;

    public function store(\Illuminate\Http\Request $request, int $id = 0) : \Totem\SamCustomers\App\Model\Group;

}