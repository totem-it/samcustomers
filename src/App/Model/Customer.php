<?php

namespace Totem\SamCustomers\App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCustomers\App\Enums\GenderType;
use Totem\SamCustomers\App\Traits\CustomerHasGroup;
use Totem\SamCustomers\App\Traits\CustomerHasCompany;
use Totem\SamAddress\App\Traits\CustomerHasAddresses;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Totem\SamCustomers\App\Model\Contracts\CustomerInterface;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
//use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * @property int id
 * @property string password
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string gender
 * @property string phone_number
 * @property int company_id
 * @property \dateTime last_login
 * @property \dateTime created_at
 * @property string fullname
 * @property string since
 * @mixin \Eloquent
 */
//class Customer extends Model implements CustomerInterface, CustomerHasGroupInterface, AuthenticatableContract, AuthorizableContract, CanResetPasswordContract, JWTSubject
class Customer extends Model implements CustomerInterface, AuthenticatableContract, CanResetPasswordContract, JWTSubject
{

//    use Authenticatable, CanResetPassword, Notifiable,
    use Authenticatable, CanResetPassword, Notifiable,
        CustomerHasGroup, CustomerHasCompany, CustomerHasAddresses,
        Authorizable, SoftDeletes;

    protected $casts = [
        'active' => 'boolean'
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'password',
            'pivot',
            'remember_token',
            'created_at',
            'updated_at'
        ]);

        $this->fillable([
            'firstname',
            'lastname',
            'email',
            'password',
            'gender',
            'phone_number',
            'active'
        ]);

        parent::__construct($attributes);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'gd' => config('sam-customers.guard-api')
        ];
    }

    public function getFullnameAttribute() : string
    {
        return "{$this->firstname} {$this->lastname}";
    }

    public function getSinceAttribute() : string
    {
        return $this->{$this->getCreatedAtColumn()}->format('d-m-Y');
    }

    public function getGenderNameAttribute() : string
    {
        return GenderType::getDescription($this->gender);
    }

}