<?php

namespace Totem\SamCustomers\App\Model;

use Illuminate\Database\Eloquent\Model;
use Totem\SamCustomers\App\Model\Contracts\CompanyInterface;

/**
 * @property int id
 * @property string nip
 * @property string name
 * @property string street
 * @property string street_number
 * @property string place_number
 * @property string post_code
 * @property string city
 * @property string country_code
 * @property string phone_number
 * @property string email
 * @property string regon
 * @property \dateTime created_at
 * @mixin \Eloquent
 */
class Company extends Model implements CompanyInterface
{

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'pivot',
            'created_at',
            'updated_at'
        ]);

        $this->fillable([
            'nip',
            'name',
            'street',
            'street_number',
            'place_number',
            'post_code',
            'city',
            'country_code',
            'phone_number',
            'email',
            'regon',
        ]);

        parent::__construct($attributes);
    }

    public function customer() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Customer::class);
    }

    public function getAddressAttribute() : string
    {
        return "{$this->street} {$this->street_number} {$this->place_number}";
    }
}