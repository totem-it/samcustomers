<?php

namespace Totem\SamCustomers\App\Model;

use Illuminate\Database\Eloquent\Model;
use Totem\SamCustomers\App\Model\Contracts\GroupInterface;

/**
 * @property int id
 * @property string code
 * @property string name
 * @mixin \Eloquent
 */
class Group extends Model implements GroupInterface
{

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at'
        ]);

        $this->fillable([
            'code',
            'name'
        ]);

        $this->setTable('customers_groups');

        parent::__construct($attributes);
    }

    public function customers() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Customer::class);
    }

}