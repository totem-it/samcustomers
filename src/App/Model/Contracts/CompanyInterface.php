<?php

namespace Totem\SamCustomers\App\Model\Contracts;

use Illuminate\Database\Eloquent\Relations\HasMany;

interface CompanyInterface
{
    /**
     * Company has many customers.
     *
     * @return HasMany
     */
    public function customer() : \Illuminate\Database\Eloquent\Relations\HasMany;
}