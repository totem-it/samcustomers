<?php

namespace Totem\SamCustomers\App\Model\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface GroupInterface
{
    /**
     * Group belongs to many customers.
     *
     * @return BelongsToMany
     */
    public function customers() : BelongsToMany;
}