<?php

namespace Totem\SamCustomers\App\Enums;

use BenSampo\Enum\Enum;

final class GenderType extends Enum
{
    public const UNKNOWN = 'u';
    public const FEMALE = 'f';
    public const MALE = 'm';
}
