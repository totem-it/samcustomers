<?php

namespace Totem\SamCustomers\App\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm() : \Illuminate\View\View
    {
        return view('sam-customers::login');
    }

    public function redirectTo() : string
    {
        return route('home');
    }

    protected function validateLogin(Request $request) : void
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|string',
        ]);
    }

    protected function attemptLogin(Request $request) : bool
    {
        return $this->guard()->attempt(
            $request->only(['email', 'password']), $request->filled('remember')
        );
    }

    protected function sendLoginResponse(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated();
    }

    protected function sendFailedLoginResponse(Request $request) : \Illuminate\Http\RedirectResponse
    {
        return redirect()->back()->withInput($request->only(['email', 'remember']))->withErrors(['email' => [__('auth.failed')]]);
    }

    protected function authenticated() : \Illuminate\Http\RedirectResponse
    {
        return redirect()->intended($this->redirectPath());
    }

    protected function guard()
    {
        return auth()->guard(config('sam-customers.guard'));
    }
}
