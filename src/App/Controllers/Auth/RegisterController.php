<?php

namespace Totem\SamCustomers\App\Controllers\Auth;

use Illuminate\Http\Request;
use Totem\SamCustomers\App\Requests\RegisterRequest;
use Totem\SamCustomers\App\Model\Company;
use Totem\SamCustomers\App\Model\Customer;
use Totem\SamCustomers\App\Repositories\CompanyRepository;
use Totem\SamCustomers\App\Repositories\CustomerRepository;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Totem\SamCore\App\Controllers\Controller;

class RegisterController extends Controller
{
    use RedirectsUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm() : \Illuminate\View\View
    {
        return view('sam-customers::register');
    }

    public function showCompanyRegistrationForm() : \Illuminate\View\View
    {
        return view('sam-customers::register-company');
    }

    public function redirectTo() : string
    {
        return route('home');
    }

    public function register(RegisterRequest $request) : \Illuminate\Http\RedirectResponse
    {
        if (!empty($request->input('nip'))) {
            $company = $this->createCompany($request);
            $request->request->add(['company_id' => $company->id]);
        }

        $customer = $this->createCustomer($request);

        $this->guard()->login($customer);

        return $this->registered();
    }

    protected function createCustomer(Request $request) : Customer
    {
        return (new CustomerRepository())->store($request);
    }

    protected function createCompany(Request $request) : Company
    {
        return (new CompanyRepository())->store($request);
    }

    protected function registered() : \Illuminate\Http\RedirectResponse
    {
        return redirect()->intended($this->redirectPath());
    }

    protected function guard()
    {
        return auth()->guard(config('sam-customers.guard'));
    }

}
