<?php

namespace Totem\SamCustomers\App\Controllers;

use Rudashi\Countries\Countries;
use Totem\SamAddress\App\Model\NullContractAddress;
use Totem\SamCustomers\App\Requests\AccountAddressRequest;
use Totem\SamCustomers\App\Repositories\Contracts\CustomerRepositoryInterface;
use Totem\SamAddress\App\Repositories\Contracts\AddressRepositoryInterface;

class AddressController
{

    /**
     * @var AddressRepositoryInterface|\Totem\SamAddress\App\Repositories\AddressRepository
     */
    private $repository;

    /**
     * @var array
     */
    private $countries;

    public function __construct(AddressRepositoryInterface $repository, Countries $countries)
    {
        $this->repository   = $repository;
        $this->countries    = $countries->filter(config('sam-customers.countries'))->all();
    }

    public function index(CustomerRepositoryInterface $customer)
    {
        return view('sam-customers::account.address.index')->with([
            'address' => new NullContractAddress(),
            'customer' => $customer->findWithRelationsById(auth()->id(), ['shippingAddress', 'company']),
            'countries' => $this->countries
        ]);
    }

    public function edit(CustomerRepositoryInterface $customer, int $id = 0) : \Illuminate\View\View
    {
        return view('sam-customers::account.address.edit')->with([
            'address' => $this->repository->findById($id),
            'customer' => $customer->findById(auth()->id()),
            'countries' => $this->countries
        ]);
    }

    public function show() : \Illuminate\Http\RedirectResponse
    {
        return redirect()->route('account.settings.address-book');
    }

    public function store(AccountAddressRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $this->repository->store($request);

        return redirect()->route('account.settings.address-book')->with('success', __('Address created successfully.'));
    }

    public function update(AccountAddressRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $this->repository->store($request, $id);

        return redirect()->route('account.settings.address-book')->with('success', __('Address updated successfully.'));
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $this->repository->delete($id);

        return redirect()->route('account.settings.address-book')->with('success', __('Address deleted successfully.'));
    }

}