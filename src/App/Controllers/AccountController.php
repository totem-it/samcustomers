<?php

namespace Totem\SamCustomers\App\Controllers;

use Totem\SamCustomers\App\Requests\AccountRequest;
use Totem\SamCustomers\App\Requests\AccountCompanyRequest;
use Totem\SamCustomers\App\Requests\AccountPasswordRequest;
use Totem\SamCustomers\App\Repositories\Contracts\CompanyRepositoryInterface;
use Totem\SamCustomers\App\Repositories\Contracts\CustomerRepositoryInterface;

class AccountController
{

    /**
     * @var CustomerRepositoryInterface|\Totem\SamCustomers\App\Repositories\CustomerRepository
     */
    private $repository;

    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function profile() : \Illuminate\View\View
    {
        return view('sam-customers::account.dashboard');
    }

    public function settings() : \Illuminate\View\View
    {
        return view('sam-customers::account.settings.index')->with([
            'customer' => $this->repository->findWithRelationsById(auth()->id(), ['company']),
            'genders' => $this->repository->getGenderList(),
        ]);
    }

    public function update(AccountRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $this->repository->storeInfo($request, auth()->id());

        return redirect()->route('account.settings')->with('success', __('Account updated successfully.'));
    }

    public function passwordUpdate(AccountPasswordRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $this->repository->storePassword($request, auth()->id());

        return redirect()->route('account.settings')->with('success', __('Password changed successfully.'));
    }

    public function companyUpdate(AccountCompanyRequest $request, CompanyRepositoryInterface $companyRepository) : \Illuminate\Http\RedirectResponse
    {
        $companyRepository->store($request, auth()->user()->company_id);

        return redirect()->route('account.settings')->with('success', __('Company data updated successfully.'));
    }

}