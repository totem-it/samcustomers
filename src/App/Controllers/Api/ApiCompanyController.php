<?php

namespace Totem\SamCustomers\App\Controllers\Api;

use Totem\SamCustomers\App\Requests\CompanyRequest;
use Totem\SamCustomers\App\Resources\CompanyResource;
use Totem\SamCustomers\App\Resources\CompanyCollection;
use Totem\SamCustomers\App\Repositories\Contracts\CompanyRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiCompanyController extends ApiController
{

    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(CompanyRequest $request) : CompanyResource
    {
        return new CompanyResource($this->repository->store($request));
    }

    public function index() : CompanyCollection
    {
        return new CompanyCollection(
            $this->getFromRequestQuery($this->repository)
        );
    }

    public function show(int $id) : CompanyResource
    {
        return new CompanyResource(
            $this->getFromRequestQuery($this->repository->findWithRelationsById($id, ['customer']))
        );
    }

    public function replace(int $id, CompanyRequest $request) : CompanyResource
    {
        return new CompanyResource($this->repository->store($request, $id));
    }

    public function destroy(int $id) : CompanyResource
    {
        return new CompanyResource($this->repository->delete($id));
    }

}