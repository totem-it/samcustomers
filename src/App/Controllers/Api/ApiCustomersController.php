<?php

namespace Totem\SamCustomers\App\Controllers\Api;

use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Totem\SamAddress\App\Resources\AddressResource;
use Totem\SamCustomers\App\Requests\CustomerRequest;
use Totem\SamCustomers\App\Resources\CompanyResource;
use Totem\SamCustomers\App\Resources\CustomerResource;
use Totem\SamCustomers\App\Resources\GroupCollection;
use Totem\SamCustomers\App\Resources\CustomerCollection;
use Totem\SamAddress\App\Resources\AddressCollection;
use Totem\SamAddress\App\Resources\ShippingAddressCollection;
use Totem\SamCustomers\App\Repositories\Contracts\CustomerRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiCustomersController extends ApiController
{

    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(CustomerRequest $request) : CustomerResource
    {
        return new CustomerResource($this->repository->store($request));
    }

    public function index() : CustomerCollection
    {
        return new CustomerCollection(
            $this->getFromRequestQuery($this->repository->with('groups'))
        );
    }

    public function show(int $id) : CustomerResource
    {
        return new CustomerResource(
            $this->getFromRequestQuery($this->repository->findWithRelationsById($id, ['groups']))
        );
    }

    public function replace(int $id, CustomerRequest $request) : CustomerResource
    {
        return new CustomerResource($this->repository->store($request, $id));
    }

    public function destroy(int $id) : CustomerResource
    {
        return new CustomerResource($this->repository->delete($id));
    }

    public function showAll(int $id) : CustomerResource
    {
        return new CustomerResource(
            $this->repository->findWithRelationsById($id, ['groups', 'company', 'addresses'])
        );
    }

    public function showCompany(int $id) : CompanyResource
    {
        return new CompanyResource(
            $this->repository->findCustomerCompany($id)
        );
    }

    public function showGroups(int $id) : GroupCollection
    {
        return new GroupCollection(
            $this->getFromRequestQuery($this->repository->findCustomerGroups($id))
        );
    }

    public function showAddresses(int $id) : AddressCollection
    {
        return new AddressCollection(
            $this->getFromRequestQuery($this->repository->findCustomerAddresses($id))
        );
    }

    public function showBillingAddresses(int $id) : AddressResource
    {
        return new AddressResource(
            $this->getFromRequestQuery($this->repository->findCustomerBillingAddress($id))
        );
    }

    public function showShippingAddresses(int $id) : ShippingAddressCollection
    {
        return new ShippingAddressCollection(
            $this->getFromRequestQuery($this->repository->findCustomerShippingAddress($id))
        );
    }

    public function update(int $id, Request $request) : \Totem\SamCore\App\Resources\ApiResource
    {
        switch ($request->get('action')) {
            case 'activate' :
                return new CustomerResource($this->repository->activate($id));
            case 'deactivate' :
                return new CustomerResource($this->repository->deactivate($id));
            default :
                return $this->response($this->error(422,'Missing Parameter.'));
        }
    }

    public function profile(AuthManager $auth) : CustomerResource
    {
        return new CustomerResource(
            $this->getFromRequestQuery(
                $this->repository->findWithRelationsById($auth->guard()->id(), ['company', 'billingAddress', 'shippingAddress'])
            )
        );
    }
}