<?php

namespace Totem\SamCustomers\App\Controllers\Api;

use Totem\SamCustomers\App\Requests\GroupRequest;
use Totem\SamCustomers\App\Resources\GroupResource;
use Totem\SamCustomers\App\Resources\GroupCollection;
use Totem\SamCustomers\App\Repositories\Contracts\GroupRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiGroupsController extends ApiController
{

    public function __construct(GroupRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(GroupRequest $request) : GroupResource
    {
        return new GroupResource($this->repository->store($request));
    }

    public function index() : GroupCollection
    {
        return new GroupCollection(
            $this->getFromRequestQuery($this->repository)
        );
    }

    public function show(int $id) : GroupResource
    {
        return new GroupResource(
            $this->getFromRequestQuery($this->repository->findWithRelationsById($id, ['customers']))
        );
    }

    public function replace(int $id, GroupRequest $request) : GroupResource
    {
        return new GroupResource($this->repository->store($request, $id));
    }

    public function destroy(int $id) : GroupResource
    {
        return new GroupResource($this->repository->delete($id));
    }

}