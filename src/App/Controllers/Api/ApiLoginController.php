<?php

namespace Totem\SamCustomers\App\Controllers\Api;

use Illuminate\Http\Request;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamCore\App\Controllers\ApiController;

class ApiLoginController extends ApiController
{

    public function __construct()
    {
        $this->middleware(config('sam-customers.guard-api'), ['except' => ['login']]);
    }

    public function login(Request $request) : ApiResource
    {
        $credentials = $request->only('email', 'password');

        if (! $token = $this->guard()->attempt($credentials)) {
            return $this->response($this->error(401, 'Unauthorized'));
        }

        return $this->respondWithToken($token);
    }

    public function refresh() : ApiResource
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    protected function respondWithToken(string $token) : ApiResource
    {
        return $this->response($this->success([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]));
    }

    public function logout() : ApiResource
    {
        $this->guard()->logout();

        return $this->response($this->success( 'Successfully logged out'));
    }

    protected function guard()
    {
        return auth()->guard(config('sam-customers.guard-api'));
    }
}
