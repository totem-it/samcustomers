<?php

namespace Totem\SamCustomers\App\Controllers\Api;

use Totem\SamCustomers\App\Model\Company;
use Totem\SamCustomers\App\Model\Customer;
use Totem\SamCustomers\App\Requests\Api\CompanyRegisterRequest;
use Totem\SamCustomers\App\Requests\Api\CustomerRegisterRequest;
use Totem\SamCustomers\App\Repositories\CompanyRepository;
use Totem\SamCustomers\App\Repositories\CustomerRepository;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamCore\App\Controllers\ApiController;

class ApiRegisterController extends ApiController
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(CustomerRegisterRequest $request) : ApiResource
    {
        $this->createCustomer($request);

        $token = $this->guard()->attempt(
            $request->only('email', 'password')
        );

        return $this->respondWithToken($token);
    }

    public function registerWithCompany(CompanyRegisterRequest $request) : ApiResource
    {
        if (!empty($request->input('nip'))) {
            $company = $this->createCompany($request);
            $request->request->add(['company_id' => $company->id]);
        }

        return $this->register($request);
    }

    protected function createCustomer(CustomerRegisterRequest $request) : Customer
    {
        return (new CustomerRepository())->store($request);
    }

    protected function createCompany(CompanyRegisterRequest $request) : Company
    {
        return (new CompanyRepository())->store($request);
    }

    protected function respondWithToken(string $token) : ApiResource
    {
        return $this->response($this->success([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]));
    }

    protected function guard()
    {
        return auth()->guard(config('sam-customers.guard-api'));
    }

}
