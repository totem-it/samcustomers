<?php

namespace Totem\SamCustomers\App\Controllers;

use Rudashi\Countries\Countries;
use Totem\SamCustomers\App\Requests\CompanyRequest;
use Totem\SamCustomers\App\Requests\CustomerRequest;
use Totem\SamCustomers\App\Repositories\Contracts\CompanyRepositoryInterface;
use Totem\SamCustomers\App\Repositories\Contracts\GroupRepositoryInterface;
use Totem\SamCustomers\App\Repositories\Contracts\CustomerRepositoryInterface;
use Totem\SamCore\App\Controllers\Controller;

class CustomersController extends Controller
{

    public function __construct(CustomerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index() : \Illuminate\View\View
    {
        return view('sam-customers::customers.index')->with([
            'customers' => $this->repository->paginateWithRelations(['groups', 'company'])
        ]);
    }

    public function create(GroupRepositoryInterface $groupRepository) : \Illuminate\View\View
    {
        return view('sam-customers::customers.create')->with([
            'groups' => $groupRepository->all(),
        ]);
    }

    public function show(int $id = 0) : \Illuminate\View\View
    {
        return view('sam-customers::customers.show')->with([
            'customer' => $this->repository->findWithRelationsById($id, ['groups', 'company']),
        ]);
    }

    public function edit(GroupRepositoryInterface $groupRepository, Countries $countries, int $id = 0) : \Illuminate\View\View
    {
        return view('sam-customers::customers.edit')->with([
            'customer' => $this->repository->findWithRelationsById($id, ['groups', 'company']),
            'groups' => $groupRepository->all(),
            'genders' => $this->repository->getGenderList(),
            'countries' => $countries->filter(config('sam-customers.countries'))->all()
        ]);
    }

    public function store(CustomerRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->store($request);

        return redirect()->route('customers.index')->with('success', __('Customer :name created successfully.', ['name' => $data->fullname]));
    }

    public function update(CustomerRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->store($request, $id);

        return redirect()->back()->with('success', __('Customer :name updated successfully.', ['name' => $data->fullname]));
    }

    public function companyUpdate(CompanyRequest $request, CompanyRepositoryInterface $companyRepository) : \Illuminate\Http\RedirectResponse
    {
        $companyRepository->store($request, $request->get('company_id'));

        return redirect()->back()->with('success', __('Company data updated successfully.'));
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->delete($id);

        return redirect()->route('customers.index')->with('success', __('Customer :name deleted successfully.', ['name' => $data->fullname]));
    }

    public function activate(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->activate($id);

        return redirect()->back()->with('success', __('Customer :name activated successfully.', ['name' => $data->fullname]));
    }

    public function deactivate(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->deactivate($id);

        return redirect()->back()->with('success', __('Customer :name locked successfully.', ['name' => $data->fullname]));
    }

}