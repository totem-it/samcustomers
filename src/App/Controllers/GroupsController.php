<?php

namespace Totem\SamCustomers\App\Controllers;

use Totem\SamCustomers\App\Requests\GroupRequest;
use Totem\SamCustomers\App\Repositories\Contracts\GroupRepositoryInterface;
use Totem\SamCore\App\Controllers\Controller;

class GroupsController extends Controller
{

    /**
     * @var GroupRepositoryInterface|\Totem\SamCustomers\App\Repositories\GroupRepository
     */
    protected $repository;

    public function __construct(GroupRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index() : \Illuminate\View\View
    {
        return view('sam-customers::groups.index')->with([
            'groups' => $this->repository->paginate()
        ]);
    }

    public function create() : \Illuminate\View\View
    {
        return view('sam-customers::groups.create');
    }

    public function show(int $id = 0) : \Illuminate\View\View
    {
        return view('sam-customers::groups.show')->with([
            'group' => $this->repository->findWithRelationsById($id, ['customers']),
        ]);
    }

    public function edit(int $id = 0) : \Illuminate\View\View
    {
        return view('sam-customers::groups.edit')->with([
            'group' => $this->repository->findById($id),
        ]);
    }

    public function store(GroupRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->store($request);

        return redirect()->route('groups.index')->with('success', __('Group :name created successfully.', ['name' => $data->name]));
    }

    public function update(GroupRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->store($request, $id);

        return redirect()->route('groups.index')->with('success', __('Group :name updated successfully.', ['name' => $data->name]));
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->repository->delete($id);

        return redirect()->route('groups.index')->with('success', __('Group :name deleted successfully.', ['name' => $data->name]));
    }

}