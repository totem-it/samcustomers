<?php

namespace Totem\SamCustomers\App\Traits;

use Totem\SamCustomers\App\Model\Group;

/**
 * @property null|\Illuminate\Database\Eloquent\Collection|Group[] groups
 */
trait CustomerHasGroup
{
    public function groups() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Group::class);
    }

    public function hasGroup($group) : bool
    {
        return $this->groups->contains(function ($value) use ($group) {
            return $group === $value->id || str_is($group, $value->name);
        });
    }

    public function attachGroup($group) : void
    {
        $this->hasGroup($group) ?: $this->groups()->attach($group);
    }

    public function attachGroups(array $groups) : void
    {
        foreach ($groups as $group) {
            $this->attachGroup($group);
        }
    }

    public function detachGroup($group) : int
    {
        return $this->groups()->detach($group);
    }

    public function detachGroups(array $groups) : void
    {
        foreach ($groups as $group) {
            $this->detachGroup($group);
        }
    }

    public function syncGroups($groups): array
    {
        return $this->groups()->sync($groups);
    }

}