<?php

namespace Totem\SamCustomers\App\Traits;

use Totem\SamCustomers\App\Model\Company;

/**
 * @property null|Company company
 */
trait CustomerHasCompany
{
    public function company() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function hasCompany() : bool
    {
        return $this->company !== null;
    }

    public function attachCompany($company) : void
    {
        if ($this->hasCompany() === false) {
            $this->company()->associate($company);
            $this->save();
        }
    }

    public function detachCompany() : void
    {
        if ($this->hasCompany()) {
            $this->company()->dissociate();
            $this->save();
        }
    }

}