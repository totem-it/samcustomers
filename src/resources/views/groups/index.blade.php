@extends('sam-customers::groups.panel')

@section('title', __('Customers Groups'))

@section('content')
    <div class="row mb-3">
        <div id="DT-A" class="col-sm-8 col-xs-12">
            @permission('customer-groups.modify')
            <a class="btn btn-success" href="{{ route('groups.create') }}" title="{{ __('Create New') }}">
                <i class="fa fa-plus-circle"></i> <span>{{ __('Create New') }}</span>
            </a>
            @endpermission
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table id="permissions-table" class="table table-striped table-hover table-bordered">
                    <thead class="thead-primary">
                        <tr>
                            <th>{{ __('Code') }}</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                        <tr>
                            <td>{{ $group->code }}</td>
                            <td>{{ $group->name }}</td>
                            <td>
                                <a class="btn btn-sm btn-info" href="{{ route('groups.show', $group->id) }}" title="{{ __('Show') }}">
                                    <i class="fa fa-fw fa-eye"></i>
                                </a>
                                @permission('customer-groups.modify')
                                <a class="btn btn-sm btn-warning" href="{{ route('groups.edit', $group->id) }}" title="{{ __('Edit') }}">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <form method="POST" action="{{ route('groups.destroy', $group->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger"  title="{{ __('Delete') }}"><i class="fa fa-fw fa-times-circle"></i></button>
                                </form>
                                @endpermission
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="filters">
                        {{ $groups->total() }} <i class="fa fa-filter"></i>
                    </div>
                    <div class="ml-auto">
                        {{ $groups->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('filters')
    @parent

@endsection