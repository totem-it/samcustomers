@extends('sam-customers::groups.panel')

@section('title', __('Edit group').' '. $group->name)

@section('content-header')
    @component('sam-admin::layout.content-header')
        @slot('title')
            {{ __('Edit group') }}
        @endslot
        @slot('description')
            {{ $group->name }}
        @endslot
        @slot('breadcrumb')
            @component('sam-admin::layout.content-header-breadcrumb', [ 'data' => [ 'Customer Groups' => 'groups.index', 'Editing' => '' ] ] )@endcomponent
        @endslot
    @endcomponent
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-6">
            <div class="box">
                <form method="POST" action="{{ route('groups.update', $group->id) }}">
                    @csrf
                    @method('patch')
                    <div class="box-body">
                        <div class="row form-group">
                            <label for="code" class="col-sm-3 col-form-label">{{ __('Code') }}</label>
                            <div class="col-sm-7">
                                <input class="form-control" type="text" id="code" name="code"  value="{{ $group->code }}" disabled>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="name" class="col-sm-3 col-form-label">{{ __('Name') }}</label>
                            <div class="col-sm-7">
                                <input class="form-control required" type="text" id="name" name="name" placeholder="{{ __('Name') }}" value="{{ $group->name ?: old('name') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <a class="btn btn-danger mr-auto" href="{{ route('groups.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> {{ __('Confirm') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')

@endpush