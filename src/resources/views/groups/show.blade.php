@extends('sam-customers::groups.panel')

@section('title', __('Preview of the group').' '.$group->name)

@section('content-header')
    @component('sam-admin::layout.content-header')
        @slot('title')
            {{ __('Preview of the group') }}
        @endslot
        @slot('description')
            {{ $group->name }}
        @endslot
        @slot('breadcrumb')
            @component('sam-admin::layout.content-header-breadcrumb', [ 'data' => [ 'Customer Groups' => 'groups.index', 'Show' => '' ] ] )@endcomponent
        @endslot
    @endcomponent
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-6">
            <div class="box">
                <div class="box-body">
                    <div class="row form-group">
                        <label for="code" class="col-sm-3 col-form-label">{{ __('Code') }}</label>
                        <div class="col-sm-7">
                            <input class="form-control-plaintext" type="text" id="code" value="{{ $group->code }}" readonly>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="name" class="col-sm-3 col-form-label">{{ __('Name') }}</label>
                        <div class="col-sm-7">
                            <input class="form-control-plaintext" type="text" id="name" value="{{ __($group->name) }}" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <span class="col-sm-3 col-form-label">{{ __('Customers') }}</span>
                        <div class="col-sm-7">
                            <div class="form-control-plaintext">
                                <ul class="list-unstyled">
                                    @foreach($group->customers as $customer)
                                        <li>
                                            @permission('customers.show')
                                            <a href="{{ route('customers.show', $customer->id) }}" class="badge badge-secondary"><i class="fa fa-user"></i> {{ $customer->fullname }}</a>
                                            @else
                                            <span class="badge badge-secondary"><i class="fa fa-user"></i> {{ $customer->fullname }}</span>
                                            @endpermission
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <a class="btn btn-danger mr-auto" href="{{ route('groups.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                        @permission('customer-groups.modify')
                        <a class="btn btn-warning" href="{{ route('groups.edit', $group->id) }}"><i class="fa fa-pencil"></i> {{ __('Edit') }}</a>
                        @endpermission
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')

@endpush