<form method="POST" action="{{ route('account.settings.update.company') }}">
    @method('PATCH')
    @csrf
    <div class="card-body">
        <h2>{{ __('Company Information') }}</h2>
        <hr>
        <div class="row form-group">
            <label for="nip" class="col-sm-3 col-form-label">{{ __('NIP') }}</label>
            <div class="col-sm-7">
                <div class="input-group">
                    <input class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}" type="text" name="nip" id="nip" value="{{ old('nip') ?: $customer->company->nip }}" >
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="download-gus" data-tip="{{ __('Other company information will be downloaded automatically.') }}">
                            <i class="fa fa-download"></i> {{ __('Download from GUS') }}
                        </button>
                    </div>
                </div>
            </div>
            @if ($errors->has('nip'))
                <small class="form-text text-danger">{{ $errors->first('nip') }}</small>
            @endif
        </div>
        <div class="row form-group">
            <label for="name" class="col-sm-3 col-form-label">{{ __('Name') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" id="name" name="name" value="{{ old('name') ?: $customer->company->name }}" required>
                @if ($errors->has('name'))
                    <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="street" class="col-sm-3 col-form-label">{{ __('Street') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" type="text" id="street" name="street" value="{{ old('street') ?: $customer->company->street }}" required>
                @if ($errors->has('street'))
                    <small class="form-text text-danger">{{ $errors->first('street') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="street_number" class="col-sm-3 col-form-label">{{ __('Street number') }}</label>
            <div class="col-sm-2">
                <input class="form-control {{ $errors->has('street_number') ? 'is-invalid' : '' }}" type="text" id="street_number" name="street_number"  value="{{ old('street_number') ?: $customer->company->street_number }}" required>
                @if ($errors->has('street_number'))
                    <small class="form-text text-danger">{{ $errors->first('street_number') }}</small>
                @endif
            </div>
            <label for="place_number" class="col-sm-3 col-form-label text-right">{{ __('Place number') }}</label>
            <div class="col-sm-2">
                <input class="form-control {{ $errors->has('place_number') ? 'is-invalid' : '' }}" type="text" id="place_number" name="place_number"  value="{{ old('place_number') ?: $customer->company->place_number }}">
                @if ($errors->has('place_number'))
                    <small class="form-text text-danger">{{ $errors->first('place_number') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="post_code" class="col-sm-3 col-form-label">{{ __('Post code') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('post_code') ? 'is-invalid' : '' }}" type="text" id="post_code" name="post_code" value="{{ old('post_code') ?: $customer->company->post_code }}" required>
                @if ($errors->has('post_code'))
                    <small class="form-text text-danger">{{ $errors->first('post_code') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="city" class="col-sm-3 col-form-label">{{ __('City') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" type="text" id="city" name="city" value="{{ old('city') ?: $customer->company->city }}" required>
                @if ($errors->has('city'))
                    <small class="form-text text-danger">{{ $errors->first('city') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="country" class="col-sm-3 col-form-label">{{ __('Country') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('country') ? 'is-invalid' : '' }}" type="text" id="country" name="country" value="{{ old('country') ?: $customer->company->country }}" required>
                @if ($errors->has('country'))
                    <small class="form-text text-danger">{{ $errors->first('country') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="email" class="col-sm-3 col-form-label">{{ __('Email Address') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" id="email" name="email"  value="{{ old('email') ?: $customer->company->email }}" required>
                @if ($errors->has('email'))
                    <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="regon" class="col-sm-3 col-form-label">{{ __('Regon') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('regon') ? 'is-invalid' : '' }}" type="text" id="regon" name="regon" value="{{ old('regon') ?: $customer->company->regon }}">
                @if ($errors->has('regon'))
                    <small class="form-text text-danger">{{ $errors->first('regon') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="phone_number" class="col-sm-3 col-form-label">{{ __('Phone number') }}</label>
            <div class="col-sm-7">
                <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" type="tel" id="phone_number" name="phone_number" value="{{ old('phone_number') ?: $customer->company->phone_number }}" required>
                @if ($errors->has('phone_number'))
                    <small class="form-text text-danger">{{ $errors->first('phone_number') }}</small>
                @endif
            </div>
        </div>
        <hr>
        <div class="text-right">
            <button class="btn btn-primary">
                {{ __('Confirm') }}
            </button>
        </div>
    </div>
</form>


@push('scripts-inline')
    <script>
        $(function() {

            let fields = {
                    nip : document.getElementsByName('nip')[0],
                    name : document.getElementsByName('name')[0],
                    street : document.getElementsByName('street')[0],
                    street_number : document.getElementsByName('street_number')[0],
                    place_number : document.getElementsByName('place_number')[0],
                    post_code : document.getElementsByName('post_code')[0],
                    city : document.getElementsByName('city')[0],
                    country : document.getElementsByName('country')[0],
                    email : document.getElementsByName('email')[0],
                    regon : document.getElementsByName('regon')[0],
                    phone_number : document.getElementsByName('phone_number')[0],
                },
                removeInvalidData = function () {
                    document.querySelectorAll('.form-control').forEach(
                        e => e.classList.remove('is-invalid')
                    );
                    document.querySelectorAll('.form-group .form-text').forEach(
                        e => e.parentNode.removeChild(e)
                    );
                };


            $('form').on('keyup', '.form-control.is-invalid', function(){
                removeInvalidData();
            });

            document.getElementById('download-gus').onclick = function(){
                let _this = this,
                    btnText = _this.innerHTML;
                if (fields.nip.value !== '') {
                    _this.innerHTML = '<i class="fa fa-spinner fa-spin"></i> {{ __('Loading...') }}';

                    let xhr = new XMLHttpRequest();
                    xhr.open('GET', '{{ route('api.gus.company') }}?nip='+fields.nip.value, true);
                    xhr.send();
                    xhr.onload = function() {
                        let response = JSON.parse(xhr.responseText);
                        if (xhr.status === 200) {
                            for (let key in fields) {
                                if (fields.hasOwnProperty(key)) {
                                    fields[key].value = response.data[key];
                                }
                            }
                            removeInvalidData();
                            _this.innerHTML = btnText;
                        } else {
                            let template = document.createElement("template");
                            template.innerHTML = '<small class="form-text text-danger">'+response.error.message+'</small>';
                            fields.nip.closest('.form-group').appendChild(template.content.firstChild);
                            _this.innerHTML = btnText;
                        }
                    };
                } else {
                    fields.nip.focus();
                }
            };

        });
    </script>
@endpush