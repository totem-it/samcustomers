<form method="POST" action="{{ route('account.settings.update') }}">
    @method('PATCH')
    @csrf
    <div class="card-body">
        <h2>{{ __('Your profile') }}</h2>
        <hr>
        <div class="row form-group">
            <label for="firstname" class="col-sm-4 col-form-label">{{ __('First name') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}" type="text" id="firstname" name="firstname" value="{{ old('firstname') ?: $customer->firstname }}" required>
                @if ($errors->has('firstname'))
                    <small class="form-text text-danger">{{ $errors->first('firstname') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="lastname" class="col-sm-4 col-form-label">{{ __('Last name') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}" type="text" id="lastname" name="lastname" value="{{ old('lastname') ?: $customer->lastname }}" required>
                @if ($errors->has('lastname'))
                    <small class="form-text text-danger">{{ $errors->first('lastname') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="email-account" class="col-sm-4 col-form-label">{{ __('Email Address') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{ $errors->has('email-account') ? 'is-invalid' : '' }}" type="email" id="email-account" name="email-account"  value="{{ old('email-account') ?: $customer->email }}" required>
                @if ($errors->has('email-account'))
                    <small class="form-text text-danger">{{ $errors->first('email-account') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="phone_number-account" class="col-sm-4 col-form-label">{{ __('Phone number') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{ $errors->has('phone_number-account') ? 'is-invalid' : '' }}" type="tel" id="phone_number-account" name="phone_number-account" value="{{ old('phone_number-account') ?: $customer->phone_number }}" required>
                @if ($errors->has('phone_number-account'))
                    <small class="form-text text-danger">{{ $errors->first('phone_number-account') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <label for="gender" class="col-sm-4 col-form-label">{{ __('Gender') }}</label>
            <div class="col-sm-4">
                <select name="gender" id="gender" class="form-control" required>
                    @foreach ($genders as $code => $gender)
                        <option value="{{ $code }}" {{ $customer->gender === $code ? 'selected' : '' }}>{{ __($gender) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="text-right">
            <button class="btn btn-primary">
                {{ __('Confirm') }}
            </button>
        </div>
    </div>
</form>