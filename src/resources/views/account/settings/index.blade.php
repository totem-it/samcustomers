@extends('sam-customers::account.index')

@section('container')

    @include('sam-customers::account.header')

    @include('sam-customers::account.flash-message')

    <div class="row">
        <div class="col-3">

            @include('sam-customers::account.menu')

        </div>
        <div class="col">
            <div class="card">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-info">
                        @include('sam-customers::account.settings.information')
                    </div>
                    <div class="tab-pane fade" id="tab-password">
                        @include('sam-customers::account.settings.password')
                    </div>
                    @if ($customer->company)
                    <div class="tab-pane fade" id="tab-company">
                        @include('sam-customers::account.settings.company')
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>


    </script>
@endpush