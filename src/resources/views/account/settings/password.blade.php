<form method="POST" action="{{ route('account.settings.update.password') }}">
    @method('PATCH')
    @csrf
    <div class="card-body">
        <h2>{{ __('Change your password') }}</h2>
        <hr>
        <div class="row form-group">
            <label for="current-password" class="col-sm-4 col-form-label">{{ __('Current password') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{ $errors->has('current-password') ? 'is-invalid' : '' }}" type="password" id="current-password" name="current-password" required autocomplete="new-password">
                @if ($errors->has('current-password'))
                    <small class="form-text text-danger">{{ $errors->first('current-password') }}</small>
                @endif
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <label for="password" class="col-sm-4 col-form-label">{{ __('New password') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" id="password" name="password" required autocomplete="new-password">
                @if ($errors->has('password'))
                    <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                @endif
                <div class="progress-bar_wrap">
                    <div class="progress-bar_item progress-bar_item-1"></div>
                    <div class="progress-bar_item progress-bar_item-2"></div>
                    <div class="progress-bar_item progress-bar_item-3"></div>
                </div>
                <span class="progress-bar_text"></span>
            </div>
        </div>
        <div class="row form-group">
            <label for="confirm-password" class="col-sm-4 col-form-label">{{ __('Repeat new password') }}</label>
            <div class="col-sm-6">
                <input class="form-control {{$errors->has('confirm-password') ? 'is-invalid' : '' }}" type="password" id="confirm-password" name="confirm-password" required autocomplete="new-password">
                @if ($errors->has('confirm-password'))
                    <small class="form-text text-danger">{{ $errors->first('confirm-password') }}</small>
                @endif
            </div>
        </div>
        <hr>
        <div class="text-right">
            <button class="btn btn-primary">
                {{ __('Change password') }}
            </button>
        </div>
    </div>
</form>

@push('scripts-inline')
    <script>
        let changeText = function (el, text, color) {
                el.innerHTML = text;
                el.style.color = color;
            },
            pbText = document.getElementsByClassName('progress-bar_text')[0];

        $('#password').keyup(function(){
            let validity = 0;


            if (this.value.length > 5) {
                if (this.value.toUpperCase() !== this.value) {
                    validity += 0.33;
                }
                if (this.value.toLowerCase() !== this.value) {
                    validity += 0.33;
                }
                if (/[0-9]/.test(this.value)) {
                    validity += 0.33;
                }
                if (/[!.@#\$%\^&\*]/.test(this.value)) {
                    validity += 0.01;
                }
            } else if (this.value.length !== 0) {
                validity = 0.25;
            }

            if (validity === 0) {
                document.querySelectorAll('.progress-bar_item').forEach(
                    e => e.classList.remove('active')
                );
                changeText(pbText, '{{ __('Security level of your password.') }}');
            } else if (validity < 0.66 ) {
                document.querySelectorAll('.progress-bar_item').forEach(
                    e => e.classList.remove('active')
                );
                document.getElementsByClassName('progress-bar_item')[0].classList.add('active');
                changeText(pbText, '{{ __('Too weak') }}');
            } else if (validity > 0.65 && validity < 0.99) {
                document.querySelectorAll('.progress-bar_item').forEach(
                    e => e.classList.remove('active')
                );
                document.getElementsByClassName('progress-bar_item')[0].classList.add('active');
                document.getElementsByClassName('progress-bar_item')[1].classList.add('active');
                changeText(pbText, '{{ __('Could be stronger') }}');
            } else {
                document.querySelectorAll('.progress-bar_item').forEach(
                    e => e.classList.add('active')
                );
                changeText(pbText, validity === 1 ? '{{ __('Very strong password') }}' : '{{ __('Strong password') }}');
            }
        });

    </script>
@endpush