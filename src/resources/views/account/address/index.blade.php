@extends('sam-customers::account.index')

@section('container')

    @include('sam-customers::account.header')

    @include('sam-customers::account.flash-message')


    <div class="row">
        <div class="col-3">

            @include('sam-customers::account.menu')

        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="mb-4">
                        <button class="btn btn-outline-primary" type="button" data-toggle="collapse" data-target=".multi-collapse">{{ __('Add new address') }}</button>
                    </div>
                    <div class="collapse multi-collapse">
                        <div class="card">
                            <form method="POST" action="{{ route('account.settings.address-book.store') }}">
                                @include('sam-customers::account.address.form', ['confirmButton' => 'Save new address'])
                            </form>
                        </div>
                    </div>
                    <div class="collapse multi-collapse show">
                        @foreach($customer->shippingAddress->chunk(2) as $chunkAddress)
                            <div class="row {{ $loop->last ? '' : 'mb-4' }}">
                                @foreach($chunkAddress as $address)
                                    <div class="col-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="card-text">{{ $address->fullname }}</div>
                                                <div class="card-text">{{ $address->address }}</div>
                                                <div class="card-text">{{ "{$address->post_code} {$address->city}" }}</div>
                                                <p class="card-text">{{ $address->phone_number }}</p>
                                                <div class="row address-delete-{{ $address->id }} collapse">
                                                    <div class="col-6">
                                                        <form method="post" action="{{ route('account.settings.address-book.destroy', $address->id) }}">
                                                            @method('DELETE')
                                                            @csrf
                                                            <button class="btn btn-link card-link text-success">{{ mb_strtoupper(__('Delete')) }}</button>
                                                        </form>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <button class="btn btn-link card-link text-danger" data-toggle="collapse" data-target=".address-delete-{{ $address->id }}" >{{ mb_strtoupper(__('No')) }}</button>
                                                    </div>
                                                </div>
                                                <div class="row address-delete-{{ $address->id }} collapse show">
                                                    <div class="col-6">
                                                        <a href="{{ route('account.settings.address-book.edit', $address->id) }}" class="card-link">{{ mb_strtoupper(__('Edit')) }}</a>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <button class="btn btn-link card-link" data-toggle="collapse" data-target=".address-delete-{{ $address->id }}">{{ mb_strtoupper(__('Delete')) }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>

    </script>
@endpush