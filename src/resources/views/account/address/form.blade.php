@csrf
<input type="hidden" name="addressable_type" value="ShippingAddress">
<div class="card-body">
    <h2>{{ __('Address Details') }}</h2>
    <hr>
    <div class="row form-group">
        <label for="firstname" class="col-sm-3 col-form-label">{{ __('First name') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}" type="text" id="firstname" name="firstname" value="{{ old('firstname') ?: $address->firstname }}" required>
            @if ($errors->has('firstname'))
                <small class="form-text text-danger">{{ $errors->first('firstname') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="lastname" class="col-sm-3 col-form-label">{{ __('Last name') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}" type="text" id="lastname" name="lastname" value="{{ old('lastname') ?: $address->lastname }}" required>
            @if ($errors->has('lastname'))
                <small class="form-text text-danger">{{ $errors->first('lastname') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="company" class="col-sm-3 col-form-label">{{ __('Company') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('company') ? 'is-invalid' : '' }}" type="text" id="company" name="company" value="{{ old('company') ?: $address->company }}">
            @if ($errors->has('company'))
                <small class="form-text text-danger">{{ $errors->first('company') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="street" class="col-sm-3 col-form-label">{{ __('Street') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" type="text" id="street" name="street" value="{{ old('street') ?: $address->street }}" required>
            @if ($errors->has('street'))
                <small class="form-text text-danger">{{ $errors->first('street') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="street_number" class="col-sm-3 col-form-label">{{ __('Street number') }}</label>
        <div class="col-sm-2">
            <input class="form-control {{ $errors->has('street_number') ? 'is-invalid' : '' }}" type="text" id="street_number" name="street_number"  value="{{ old('street_number') ?: $address->street_number }}" required>
            @if ($errors->has('street_number'))
                <small class="form-text text-danger">{{ $errors->first('street_number') }}</small>
            @endif
        </div>
        <label for="place_number" class="col-sm-3 col-form-label text-right">{{ __('Place number') }}</label>
        <div class="col-sm-2">
            <input class="form-control {{ $errors->has('place_number') ? 'is-invalid' : '' }}" type="text" id="place_number" name="place_number"  value="{{ old('place_number') ?: $address->place_number }}">
            @if ($errors->has('place_number'))
                <small class="form-text text-danger">{{ $errors->first('place_number') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="post_code" class="col-sm-3 col-form-label">{{ __('Post code') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('post_code') ? 'is-invalid' : '' }}" type="text" id="post_code" name="post_code" value="{{ old('post_code') ?: $address->post_code }}" required>
            @if ($errors->has('post_code'))
                <small class="form-text text-danger">{{ $errors->first('post_code') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="city" class="col-sm-3 col-form-label">{{ __('City') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" type="text" id="city" name="city" value="{{ old('city') ?: $address->city }}" required>
            @if ($errors->has('city'))
                <small class="form-text text-danger">{{ $errors->first('city') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="country_code" class="col-sm-3 col-form-label">{{ __('Country') }}</label>
        <div class="col-sm-7">
            <select class="form-control" id="country_code" name="country_code">
                @foreach ($countries as $country)
                    <option value="{{ $country->getCode() }}" {{ $country->getCode() === $address->country_code ? 'selected':'' }}>{{ __($country->getName()) }}</option>
                @endforeach
            </select>
            @if ($errors->has('country'))
                <small class="form-text text-danger">{{ $errors->first('country_code') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="phone_number" class="col-sm-3 col-form-label">{{ __('Phone number') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" type="tel" id="phone_number" name="phone_number" value="{{ old('phone_number') ?: $address->phone_number }}" required>
            @if ($errors->has('phone_number'))
                <small class="form-text text-danger">{{ $errors->first('phone_number') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <label for="email" class="col-sm-3 col-form-label">{{ __('Email Address') }}</label>
        <div class="col-sm-7">
            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" id="email" name="email"  value="{{ old('email') ?: $address->email }}" required>
            @if ($errors->has('email'))
                <small class="form-text text-danger">{{ $errors->first('email') }}</small>
            @endif
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-6">
            @isset($cancelButton)
            <a class="btn btn-danger" href="{{ route('account.settings.address-book') }}">
                {{ __($cancelButton) }}
            </a>
            @endisset
        </div>
        <div class="col-6 text-right">
            <button class="btn btn-primary">
                {{ __($confirmButton) }}
            </button>
        </div>
    </div>
</div>
