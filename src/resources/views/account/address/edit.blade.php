@extends('sam-customers::account.index')

@section('container')

    @include('sam-customers::account.header')

    @include('sam-customers::account.flash-message')


    <div class="row">
        <div class="col-3">

            @include('sam-customers::account.menu')

        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="mb-4">
                        <button class="btn btn-outline-primary disabled" type="button" data-toggle="collapse" data-target=".multi-collapse" disabled>{{ __('Add new address') }}</button>
                    </div>
                    <div class="collapse multi-collapse show">
                        <div class="card">
                            <form method="POST" action="{{ route('account.settings.address-book.update', $address->id) }}">
                                @method('PATCH')
                                @include('sam-customers::account.address.form', ['confirmButton' => 'Save address', 'cancelButton' => 'Cancel'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>

    </script>
@endpush