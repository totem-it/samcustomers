@if ($message = session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>
        <i class="fa fa-check"></i>
        <span>{{ __($message) }}</span>
    </div>
@endif

@if ($message = session('error'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
        <i class="fa fa-bug"></i>
        <p>{{ __($message) }}</p>
    </div>
@endif