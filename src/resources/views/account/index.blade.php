@extends('layouts.home')

@section('content')
    <div class="container account-container">
        @yield('container')
    </div>
@endsection

@push('scripts-inline')
    <script>
        let removeInvalidData = function () {
                document.querySelectorAll('.form-control').forEach(
                    e => e.classList.remove('is-invalid')
                );
                document.querySelectorAll('.form-group .form-text').forEach(
                    e => e.parentNode.removeChild(e)
                );
            },
            activeTab = window.localStorage.getItem('activeTab'),
            url = document.location.toString();

        document.querySelectorAll('.form-control').forEach(function(elem){
            elem.addEventListener('onkeyup', function() {
                removeInvalidData()
            });
        });

        document.querySelectorAll('a[data-toggle="tab"]').forEach(function(elem){
            elem.addEventListener('click', function() {
                window.localStorage.setItem('activeTab', this.getAttribute('href'));
            });
        });

        if (activeTab) {
            $('a[data-toggle="tab"][href="' + activeTab + '"]').tab('show');
            window.localStorage.removeItem("activeTab");
        }

        if (url.match('#')) {
            $('a[data-toggle="tab"][href="#' + url.split('#')[1] + '"]').tab('show');
        }
    </script>
@endpush