@extends('sam-customers::account.index')

@section('container')
    <div class="card-deck">
        <div class="card">
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="card-img-left">
                        <div class="card-img" style="background-image: url({{ asset('images/acc_orders.jpg') }});"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="card-body">
                        <h4 class="card-title">
                            Zamówienia
                        </h4>
                        <p class="card-text">
                            Bieżące
                        </p>
                        <p class="card-text">
                            Anulowane
                        </p>
                        <p class="card-text">
                            Zrealizowane
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="card-img-left">
                        <div class="card-img" style="background-image: url({{ asset('images/acc_settings.jpg') }});"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{ __('Account') }}
                        </h4>
                        <p class="card-text">
                            <a href="{{ route('account.settings') }}">{{ __('Your data') }}</a>
                        </p>
                        <p class="card-text">
                            <a href="{{ route('account.settings.address-book') }}">{{ __('Address book') }}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-deck">
        <div class="card">
            <div class="row no-gutters">
                <div class="col-4">
                    <div class="card-img-left">
                        <div class="card-img" style="background-image: url({{ asset('images/acc_offers.jpg') }});"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="card-body">
                        <h4 class="card-title">
                            Kalkulacje
                        </h4>
                        <p class="card-text">
                            Dodaj kalkulację
                        </p>
                        <p class="card-text">
                            Twoje kalkulacje
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="row no-gutters">
                <div class="col-4 align-self-center text-center">
                    <i class="fa fa-exchange fa-5x"></i>
                </div>
                <div class="col">
                    <div class="card-body">
                        <h4 class="card-title">
                            Reklamacje
                        </h4>
                        <p class="card-text">
                            Dodaj reklamację
                        </p>
                        <p class="card-text">
                            Twoje reklamacje
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts-inline')
    <script>

    </script>
@endpush