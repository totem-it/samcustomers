<div class="card h-100">
    <nav class="list-group list-group-flush">
        <a class="list-group-item" {{ Route::is('account.settings.*') ? 'href='.route('account.settings').'#tab-info' : 'data-toggle=tab  href=#tab-info' }}>
            <i class="fa fa-info"></i>
            <span>{{ __('Your data') }}</span>
        </a>
        <a class="list-group-item" {{ Route::is('account.settings.*') ? 'href='.route('account.settings').'#tab-password' : 'data-toggle=tab  href=#tab-password' }}>
            <i class="fa fa-key"></i>
            <span>{{ __('Password') }}</span>
        </a>
        @if ($customer->company)
            <a class="list-group-item" {{ Route::is('account.settings.*') ? 'href='.route('account.settings').'#tab-company' : 'data-toggle=tab  href=#tab-company' }}>
                <i class="fa fa-building-o"></i>
                <span>{{ __('Company') }}</span>
            </a>
        @endif
        <a class="list-group-item {{ Route::is('*address-book*') ? 'active' : '' }}" href="{{ route('account.settings.address-book') }}">
            <i class="fa fa-address-book-o"></i>
            <span>{{ __('Address book') }}</span>
        </a>
    </nav>
</div>