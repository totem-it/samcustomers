<div class="row ">
    <div class="col-6">
        <ul class="nav nav-pills nav-fill">
            <li class="nav-item">
                <a class="nav-link" href="#">
                    {{ __('Orders') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('account.settings*') ? 'active' : '' }}" href="{{ route('account.settings') }}">
                    <i class="fa fa-user"></i> {{ __('Account') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    {{ __('Offers') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    {{ __('Complaints') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="col-2 ml-auto">
        <a class="btn btn-success" href="#">
            {{ __('Make a book') }}
        </a>
    </div>
</div>
<hr>
