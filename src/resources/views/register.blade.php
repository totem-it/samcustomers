@extends('layouts.home')

@section('content')
    <div class="container card-body">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <form method="post" action="{{ route('register.submit') }}">
                        <div class="card-body">
                            @csrf
                            <h3 class="mb-3">
                                {{ __('Create an account') }}
                            </h3>
                            <div class="row form-group mb-4">
                                <div class="col-6">
                                    <span>
                                        <i class="fa fa-lg fa-check-circle-o text-success"></i> {{ __('Common') }}
                                    </span>
                                </div>
                                <div class="col-6">
                                    <a href="{{ route('register.company') }}">
                                        <i class="fa fa-lg fa-circle-o"></i> {{ __('Company') }}
                                    </a>
                                </div>
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }} required" type="email" name="email" id="email" placeholder=" " value="{{ old('email') }}" required>
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                @if ($errors->has('email'))
                                    <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                            <div class="form-group mb-4 has-float-label">
                                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }} required" type="password" name="password" id="password" placeholder=" " autocomplete="new-password" required>
                                <label for="password">{{ __('Password') }}</label>
                                @if ($errors->has('password'))
                                    <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                            <hr>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }} required" type="text" name="firstname" id="firstname" placeholder=" " value="{{ old('firstname') }}" >
                                <label for="firstname">{{ __('First name') }}</label>
                                @if ($errors->has('firstname'))
                                    <small class="form-text text-danger">{{ $errors->first('firstname') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }} required" type="text" name="lastname" id="lastname" placeholder=" " value="{{ old('lastname') }}" >
                                <label for="lastname">{{ __('Last name') }}</label>
                                @if ($errors->has('lastname'))
                                    <small class="form-text text-danger">{{ $errors->first('lastname') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input {{ $errors->has('agreementTerms') ? 'is-invalid' : '' }}" type="checkbox" name="agreementTerms" value="1" id="agreementTerms" {{ old('agreementTerms') ? 'checked' : '' }} required>
                                    <label class="custom-control-label" for="agreementTerms">
                                        {{ __('I have read and accept the') }} <a href="" target="_blank">{{ __('Terms and Conditions') }}</a>.
                                    </label>
                                </div>
                                @if ($errors->has('agreementTerms'))
                                    <small class="form-text text-danger">{{ $errors->first('agreementTerms') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input {{ $errors->has('agreementProcessing') ? 'is-invalid' : '' }}" type="checkbox" name="agreementProcessing" value="1" id="agreementProcessing" {{ old('agreementProcessing') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="agreementProcessing">
                                        {{ __('I declare that I consent to the processing of my personal data for the purpose of the website.') }}
                                        <a href="#agreementProcessingData" class="small" data-toggle="collapse">{{ __('Details') }}</a>
                                    </label>
                                    <div class="collapse" id="agreementProcessingData">
                                        <p class="small">
                                            {{ __('Pursuant to the Act of August 29, 1997 on the Protection of Personal Data (Journal of Laws No. 133, item 883, as amended), completing this form, I consent to the processing of my personal data for the purposes of the website and use them for the execution of orders submitted by this site. Providing data is voluntary, but necessary for the execution of orders. I also agree to use the data for promotional purposes within the meaning of the Act of 18 July 2002 on the provision of electronic services (Journal of Laws No. 144, item 1204, as amended.) At the same time, I have the right to inspect my data, correction and removal. The administrator of personal data is the company Totem.com.pl Sp. z o.o. Sp. k.') }}
                                        </p>
                                    </div>
                                </div>
                                @if ($errors->has('agreementProcessing'))
                                    <small class="form-text text-danger">{{ $errors->first('agreementProcessing') }}</small>
                                @endif
                            </div>
                            <div class="row form-group justify-content-end">
                                <div class="col-5">
                                    <button class="btn btn-block btn-lg btn-success">
                                        {{ __('Create an account') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>
        $(function() {

            $('form').on('keyup', '.form-control.is-invalid', function(){
                $('.form-control').removeClass('is-invalid');
            });

        });
    </script>
@endpush