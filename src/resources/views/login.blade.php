@extends('layouts.home')

@section('content')
    <div class="container card-body">
        <div class="row">
            <div class="col-6">
                <div class="card h-100">
                    <form method="post" action="{{ route('login.submit') }}">
                        <div class="card-body">
                            @csrf
                            <h3 class="mb-3">
                                {{ __('Sign In') }}
                            </h3>
                            <div class="form-group">
                                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" placeholder="{{ __('Password') }}" autocomplete="new-password" required>
                                @if ($errors->has('password'))
                                    <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="remember" name="remember">
                                        <label class="custom-control-label" for="remember">
                                            {{ __('Remember me') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col text-right">
                                    <a href="{{ route('password.request') }}">
                                        {{ __('Forgot your password?') }}
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-block btn-primary">{{ __('Sign In') }}</button>
                            </div>
                            @if (config('services.google'))
                            <div class="form-group hr-text">
                                <span>{{ __('or') }}</span>
                            </div>
                            <div>
                                <button class="btn btn-block btn-outline-secondary">
                                    <i class="fa fa-fw fa-google"></i> {{ __('Login with Google Account') }}
                                </button>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-6">
                <div class="card h-100">
                    <div class="card-body">
                        <h3 class="mb-3">
                            {{ __('Register') }}
                        </h3>
                        <div class="form-group">
                            <a href="{{ route('register') }}" class="btn btn-block btn-success">
                                {{ __('Create an account') }}
                            </a>
                        </div>
                        @if (config('services.google'))
                        <div class="form-group hr-text">
                            <span>{{ __('or') }}</span>
                        </div>
                        <div>
                            <button class="btn btn-block btn-outline-secondary">
                                <i class="fa fa-fw fa-google"></i> {{ __('Sign up with Google Account') }}
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>
        $(function() {

            $('form').on('keyup', '.form-control.is-invalid', function(){
                $('.form-control').removeClass('is-invalid');
            });

        });
    </script>
@endpush