@extends('layouts.home')

@section('content')
    <div class="container card-body">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <form method="post" action="{{ route('register.submit') }}">
                        <div class="card-body">
                            @csrf
                            <h3 class="mb-3">
                                {{ __('Create an account') }}
                            </h3>
                            <div class="row form-group mb-4">
                                <div class="col-6">
                                    <a href="{{ route('register') }}">
                                        <i class="fa fa-lg fa-circle-o"></i> {{ __('Common') }}
                                    </a>
                                </div>
                                <div class="col-6">
                                    <span>
                                        <i class="fa fa-lg fa-check-circle-o text-success"></i> {{ __('Company') }}
                                    </span>
                                </div>
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }} required" type="email" name="email" id="email" placeholder=" " value="{{ old('email') }}" required>
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                @if ($errors->has('email'))
                                    <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                            <div class="form-group mb-4 has-float-label">
                                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }} required" type="password" name="password" id="password" placeholder=" " autocomplete="new-password" required>
                                <label for="password">{{ __('Password') }}</label>
                                @if ($errors->has('password'))
                                    <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }} required" type="text" name="firstname" id="firstname" placeholder=" " value="{{ old('firstname') }}" >
                                <label for="firstname">{{ __('First name') }}</label>
                                @if ($errors->has('firstname'))
                                    <small class="form-text text-danger">{{ $errors->first('firstname') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }} required" type="text" name="lastname" id="lastname" placeholder=" " value="{{ old('lastname') }}" >
                                <label for="lastname">{{ __('Last name') }}</label>
                                @if ($errors->has('lastname'))
                                    <small class="form-text text-danger">{{ $errors->first('lastname') }}</small>
                                @endif
                            </div>
                            <h6 class="font-weight-bold">{{ __('Company Information') }}</h6>
                            <hr class="mt-0">
                            <div class="form-group has-float-label">
                                <div class="input-group">
                                    <input class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}" type="text" name="nip" id="nip" placeholder=" " value="{{ old('nip') }}" >
                                    <label for="nip">{{ __('NIP') }}</label>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" id="download-gus" data-tip="{{ __('Other company information will be downloaded automatically.') }}">
                                            <i class="fa fa-download"></i> {{ __('Download from GUS') }}
                                        </button>
                                    </div>
                                </div>
                                @if ($errors->has('nip'))
                                    <small class="form-text text-danger">{{ $errors->first('nip') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" placeholder=" " value="{{ old('name') }}" >
                                <label for="name">{{ __('Name') }}</label>
                                @if ($errors->has('name'))
                                    <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('country') ? 'is-invalid' : '' }}" type="text" name="country" id="country" placeholder=" " value="{{ old('country') }}" >
                                <label for="country">{{ __('Country') }}</label>
                                @if ($errors->has('country'))
                                    <small class="form-text text-danger">{{ $errors->first('country') }}</small>
                                @endif
                            </div>
                            <hr>
                            <div class="form-group has-float-label">
                                <div class="form-row">
                                    <div class="col-8">
                                        <input class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" type="text" name="street" id="street" placeholder=" " value="{{ old('street') }}" >
                                        <label for="street">{{ __('Street') }}</label>
                                    </div>
                                    <div class="col-2">
                                        <input class="form-control {{ $errors->has('street_number') ? 'is-invalid' : '' }}" type="text" name="street_number" id="street_number" placeholder=" " value="{{ old('street_number') }}" >
                                        <label for="street_number">{{ __('Street number') }}</label>
                                    </div>
                                    <div class="col-2">
                                        <input class="form-control {{ $errors->has('place_number') ? 'is-invalid' : '' }}" type="text" name="place_number" id="place_number" placeholder=" " value="{{ old('place_number') }}" >
                                        <label for="place_number">{{ __('Place number') }}</label>
                                    </div>
                                </div>
                                @if ($errors->has('street'))
                                    <small class="form-text text-danger">{{ $errors->first('street') }}</small>
                                @endif
                                @if ($errors->has('street_number'))
                                    <small class="form-text text-danger">{{ $errors->first('street_number') }}</small>
                                @endif
                                @if ($errors->has('place_number'))
                                    <small class="form-text text-danger">{{ $errors->first('place_number') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <div class="form-row">
                                    <div class="col-3">
                                        <input class="form-control {{ $errors->has('post_code') ? 'is-invalid' : '' }}" type="text" name="post_code" id="post_code" placeholder=" " value="{{ old('post_code') }}" >
                                        <label for="post_code">{{ __('Post code') }}</label>
                                    </div>
                                    <div class="col-9">
                                        <input class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" type="text" name="city" id="city" placeholder=" " value="{{ old('city') }}" >
                                        <label for="city">{{ __('City') }}</label>
                                    </div>
                                </div>
                                @if ($errors->has('post_code'))
                                    <small class="form-text text-danger">{{ $errors->first('post_code') }}</small>
                                @endif
                                @if ($errors->has('city'))
                                    <small class="form-text text-danger">{{ $errors->first('city') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" type="text" name="phone_number" id="phone_number" placeholder=" " value="{{ old('phone_number') }}" >
                                <label for="phone_number">{{ __('Phone number') }}</label>
                                @if ($errors->has('phone_number'))
                                    <small class="form-text text-danger">{{ $errors->first('phone_number') }}</small>
                                @endif
                            </div>
                            <div class="form-group has-float-label">
                                <input class="form-control {{ $errors->has('regon') ? 'is-invalid' : '' }}" type="text" name="regon" id="regon" placeholder=" " value="{{ old('regon') }}" >
                                <label for="regon">{{ __('REGON') }}</label>
                                @if ($errors->has('regon'))
                                    <small class="form-text text-danger">{{ $errors->first('regon') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input {{ $errors->has('agreementTerms') ? 'is-invalid' : '' }}" type="checkbox" name="agreementTerms" value="1" id="agreementTerms" {{ old('agreementTerms') ? 'checked' : '' }} required>
                                    <label class="custom-control-label" for="agreementTerms">
                                        {{ __('I have read and accept the') }} <a href="" target="_blank">{{ __('Terms and Conditions') }}</a>.
                                    </label>
                                </div>
                                @if ($errors->has('agreementTerms'))
                                    <small class="form-text text-danger">{{ $errors->first('agreementTerms') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input {{ $errors->has('agreementProcessing') ? 'is-invalid' : '' }}" type="checkbox" name="agreementProcessing" value="1" id="agreementProcessing" {{ old('agreementProcessing') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="agreementProcessing">
                                        {{ __('I declare that I consent to the processing of my personal data for the purpose of the website.') }}
                                        <a href="#agreementProcessingData" class="small" data-toggle="collapse">{{ __('Details') }}</a>
                                    </label>
                                    <div class="collapse" id="agreementProcessingData">
                                        <p class="small">
                                            {{ __('Pursuant to the Act of August 29, 1997 on the Protection of Personal Data (Journal of Laws No. 133, item 883, as amended), completing this form, I consent to the processing of my personal data for the purposes of the website and use them for the execution of orders submitted by this site. Providing data is voluntary, but necessary for the execution of orders. I also agree to use the data for promotional purposes within the meaning of the Act of 18 July 2002 on the provision of electronic services (Journal of Laws No. 144, item 1204, as amended.) At the same time, I have the right to inspect my data, correction and removal. The administrator of personal data is the company Totem.com.pl Sp. z o.o. Sp. k.') }}
                                        </p>
                                    </div>
                                </div>
                                @if ($errors->has('agreementProcessing'))
                                    <small class="form-text text-danger">{{ $errors->first('agreementProcessing') }}</small>
                                @endif
                            </div>
                            <div class="row form-group justify-content-end">
                                <div class="col-5">
                                    <button class="btn btn-block btn-lg btn-success">
                                        {{ __('Create an account') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>
        $(function() {

            let fields = {
                    nip : document.getElementsByName('nip')[0],
                    name : document.getElementsByName('name')[0],
                    country : document.getElementsByName('country')[0],
                    street : document.getElementsByName('street')[0],
                    street_number : document.getElementsByName('street_number')[0],
                    place_number : document.getElementsByName('place_number')[0],
                    post_code : document.getElementsByName('post_code')[0],
                    city : document.getElementsByName('city')[0],
                    phone_number : document.getElementsByName('phone_number')[0],
                    regon : document.getElementsByName('regon')[0],
                },
                removeInvalidData = function () {
                    document.querySelectorAll('.form-control').forEach(
                        e => e.classList.remove('is-invalid')
                    );
                    document.querySelectorAll('.form-group .form-text').forEach(
                        e => e.parentNode.removeChild(e)
                    );
                };


            $('form').on('keyup', '.form-control.is-invalid', function(){
                removeInvalidData();
            });

            document.getElementById('download-gus').onclick = function(){
                let _this = this,
                    btnText = _this.innerHTML;
                if (fields.nip.value !== '') {
                    _this.innerHTML = '<i class="fa fa-spinner fa-spin"></i> {{ __('Loading...') }}';

                    let xhr = new XMLHttpRequest();
                    xhr.open('GET', '{{ route('api.gus.company') }}?nip='+fields.nip.value, true);
                    xhr.send();
                    xhr.onload = function() {
                        let response = JSON.parse(xhr.responseText);
                        if (xhr.status === 200) {
                            for (let key in fields) {
                                if (fields.hasOwnProperty(key)) {
                                    fields[key].value = response.data[key];
                                }
                            }
                            removeInvalidData();
                            _this.innerHTML = btnText;
                        } else {
                            let template = document.createElement("template");
                            template.innerHTML = '<small class="form-text text-danger">'+response.error.message+'</small>';
                            fields.nip.closest('.form-group').appendChild(template.content.firstChild);
                            _this.innerHTML = btnText;
                        }
                    };
                } else {
                    fields.nip.focus();
                }
            };

        });
    </script>
@endpush