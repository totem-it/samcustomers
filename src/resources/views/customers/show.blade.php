@extends('sam-customers::customers.panel')

@section('title', __('Preview of the customer').' '.$customer->fullname)

@section('content-header')
    @component('sam-admin::layout.content-header')
        @slot('title')
            {{ __('Preview of the customer') }}
        @endslot
        @slot('description')
            {{ $customer->fullname }}
        @endslot
        @slot('breadcrumb')
            @component('sam-admin::layout.content-header-breadcrumb', [ 'data' => [ 'Customers' => 'customers.index', 'Show' => '' ] ] )@endcomponent
        @endslot
    @endcomponent
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-3">
                            <h4 class="text-center mt-3">
                                {{ $customer->fullname }}
                            </h4>
                            @if (!$customer->active)
                                <div class="text-center text-danger">{{ __('disabled') }}</div>
                            @endif
                            <p>
                                <i class="fa fa-fw fa-envelope"></i> {{ $customer->email }}
                            </p>
                            <p>
                                <i class="fa fa-fw fa-phone"></i> {{ $customer->phone_number }}
                            </p>
                            <p>
                                <i class="fa fa-fw fa-user-plus"></i> {{ $customer->since }}
                            </p>
                            <p>
                            @foreach ($customer->groups as $group)
                                @permission('customer-groups.view')
                                    <a href="{{ route('groups.show', $group->id) }}" class="badge badge-success">{{ $group->name }}</a>
                                @else
                                    <span class="badge badge-success">{{ $group->name }}</span>
                                @endpermission
                            @endforeach
                            </p>
                            @permission('customers.edit|customers.delete')
                            <hr>
                            @permission('customers.edit')
                            <p>
                                <a href="{{ route('customers.edit', $customer->id) }}" class="btn btn-sm btn-block btn-warning">{{ __('Edit') }}</a>
                            </p>
                            @endpermission
                            @permission('customers.delete')
                            <form method="post" action="{{ route('customers.destroy', $customer->id) }}">
                                <p>
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-block btn-danger">{{ __('Delete') }}</button>
                                </p>
                            </form>
                            @endpermission
                            @permission('customers.edit')
                            @if ($customer->active)
                                <form method="POST" action="{{ route('customers.deactivation', $customer->id) }}">
                                    @csrf
                                    @method('PATCH')
                                    <button class="btn btn-sm btn-block btn-default">{{ __('Lock') }}</button>
                                </form>
                            @else
                                <form method="POST" action="{{ route('customers.activation', $customer->id) }}">
                                    @csrf
                                    @method('PATCH')
                                    <button class="btn btn-sm btn-block btn-default">{{ __('Activate') }}</button>
                                </form>
                            @endif
                            @endpermission
                            @endpermission
                        </div>
                        <div class="col-9">
                            <div class="nav-tabs-inside">
                                <nav class="nav nav-tabs" id="nav-tab">
                                    <a class="nav-item nav-link active" data-toggle="tab" href="#info">
                                        <i class="fa fa-info"></i>
                                        <span>{{ __('Information') }}</span>
                                    </a>
                                    @if ($customer->company)
                                    <a class="nav-item nav-link" data-toggle="tab" href="#company">
                                        <i class="fa fa-building-o"></i>
                                        <span>{{ __('Company') }}</span>
                                    </a>
                                    @else
                                    <span class="nav-item nav-link">
                                        <i class="fa fa-building-o"></i>
                                        <span>{{ __('Company') }}</span>
                                    </span>
                                    @endif
                                    <a class="nav-item nav-link" data-toggle="tab" href="#address">
                                        <i class="fa fa-address-book-o"></i>
                                        <span>{{ __('Address book') }}</span>
                                    </a>
                                </nav>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="info">
                                        @include('sam-customers::customers.show.information')
                                    </div>
                                    @if ($customer->company)
                                    <div class="tab-pane fade" id="company">
                                        @include('sam-customers::customers.show.company')
                                    </div>
                                    @endif
                                    <div class="tab-pane fade" id="address">
                                        @include('sam-customers::customers.show.address')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')

@endpush