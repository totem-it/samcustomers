@extends('sam-customers::customers.panel')

@section('title', __('Customers'))

@section('content')
    <div class="row mb-3">
        <div id="DT-A" class="col-sm-8 col-xs-12">
            @permission('customers.create')
            <a class="btn btn-success" href="{{ route('customers.create') }}" title="{{ __('Create New') }}">
                <i class="fa fa-plus-circle"></i> <span>{{ __('Create New') }}</span>
            </a>
            @endpermission
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table id="permissions-table" class="table table-striped table-hover table-bordered">
                    <thead class="thead-primary">
                        <tr>
                            <th>{{ __('First name') }}</th>
                            <th>{{ __('Last name') }}</th>
                            <th>{{ __('Email') }}</th>
                            <th>{{ __('Registration date') }}</th>
                            <th>{{ __('Groups') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $customer)
                        <tr {{ !$customer->active ? 'class=inactive' : '' }}>
                            <td>{{ $customer->firstname }}</td>
                            <td>{{ $customer->lastname }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->created_at }}</td>
                            <td>
                                @foreach ($customer->groups as $group)
                                    @permission('customer-groups.view')
                                        <a href="{{ route('groups.show', $group->id) }}" class="badge badge-success">{{ $group->name }}</a>
                                    @else
                                        <span class="badge badge-success">{{ $group->name }}</span>
                                    @endpermission
                                @endforeach
                            </td>
                            <td>
                                @permission('customers.show')
                                <a class="btn btn-sm btn-info" href="{{ route('customers.show', $customer->id) }}" title="{{ __('Show') }}">
                                    <i class="fa fa-fw fa-eye"></i>
                                </a>
                                @if ($customer->active)
                                    <form method="POST" action="{{ route('customers.deactivation', $customer->id) }}" data-inactive="false">
                                        @csrf
                                        @method('PATCH')
                                        <button class="btn btn-sm btn-default" title="{{ __('Lock') }}"><i class="fa fa-fw fa-lock"></i></button>
                                    </form>
                                @else
                                    <form method="POST" action="{{ route('customers.activation', $customer->id) }}" data-inactive="false">
                                        @csrf
                                        @method('PATCH')
                                        <button class="btn btn-sm btn-default" title="{{ __('Activate') }}"><i class="fa fa-fw fa-unlock-alt"></i></button>
                                    </form>
                                @endif
                                @endpermission
                                @permission('customers.edit')
                                <a class="btn btn-sm btn-warning" href="{{ route('customers.edit', $customer->id) }}" title="{{ __('Edit') }}">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                @endpermission
                                @permission('customers.delete')
                                <form method="POST" action="{{ route('customers.destroy', $customer->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger"  title="{{ __('Delete') }}"><i class="fa fa-fw fa-times-circle"></i></button>
                                </form>
                                @endpermission
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="filters">
                        {{ $customers->total() }} <i class="fa fa-filter"></i>
                    </div>
                    <div class="ml-auto">
                        {{ $customers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('filters')
    @parent

@endsection