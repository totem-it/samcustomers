@extends('sam-customers::customers.panel')

@section('title', __('Edit customer').' '.$customer->fullname)

@section('content-header')
    @component('sam-admin::layout.content-header')
        @slot('title')
            {{ __('Edit customer') }}
        @endslot
        @slot('description')
            {{ $customer->fullname }}
        @endslot
        @slot('breadcrumb')
            @component('sam-admin::layout.content-header-breadcrumb', [ 'data' => [ 'Customers' => 'customers.index', 'Editing' => '' ] ] )@endcomponent
        @endslot
    @endcomponent
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="box nav-tabs">
                <nav class="nav nav-tabs" id="nav-tab">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#info">
                        <i class="fa fa-info"></i>
                        <span>{{ __('Information') }}</span>
                    </a>
                    @if ($customer->hasCompany())
                    <a class="nav-item nav-link" data-toggle="tab" href="#company">
                        <i class="fa fa-building-o"></i>
                        <span>{{ __('Company') }}</span>
                    </a>
                    @else
                    <span class="nav-item nav-link">
                        <i class="fa fa-building-o"></i>
                        <span>{{ __('Company') }}</span>
                    </span>
                    @endif
                    @if ($customer->has_shipping_address)
                        <a class="nav-item nav-link" data-toggle="tab" href="#address">
                            <i class="fa fa-address-book-o"></i>
                            <span>{{ __('Address book') }}</span>
                        </a>
                    @else
                        <span class="nav-item nav-link">
                            <i class="fa fa-address-book-o"></i>
                            <span>{{ __('Address book') }}</span>
                    </span>
                    @endif
                    @if ($customer->has_billing_address)
                        <a class="nav-item nav-link" data-toggle="tab" href="#payment">
                            <i class="fa fa-dollar"></i>
                            <span>{{ __('Payment Data') }}</span>
                        </a>
                    @else
                        <span class="nav-item nav-link">
                            <i class="fa fa-dollar"></i>
                            <span>{{ __('Payment Data') }}</span>
                    </span>
                    @endif

                </nav>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="info">
                        @include('sam-customers::customers.edit.information')
                    </div>
                    @if ($customer->hasCompany())
                    <div class="tab-pane fade" id="company">
                        @include('sam-customers::customers.edit.company')
                    </div>
                    @endif
                    @if ($customer->has_shipping_address)
                    <div class="tab-pane fade" id="address">
                        @include('sam-customers::customers.show.address')
                    </div>
                    @endif
                    @if ($customer->has_billing_address)
                    <div class="tab-pane fade" id="address">

                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')

@endpush