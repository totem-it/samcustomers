@extends('sam-customers::customers.panel')

@section('title', __('Create a new customer'))

@section('content-header')
    @component('sam-admin::layout.content-header')
        @slot('title')
            {{ __('Create a new customer') }}
        @endslot
        @slot('breadcrumb')
            @component('sam-admin::layout.content-header-breadcrumb', [ 'data' => [ 'Customers' => 'customers.index', 'Creating' => '' ] ] )@endcomponent
        @endslot
    @endcomponent
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-9">
            <div class="box">
                <form method="POST" action="{{ route('customers.store') }}">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="card">
                                    <div class="box-body">
                                        <div class="row form-group">
                                            <label for="firstname" class="col-sm-4 col-form-label">{{ __('First name') }}</label>
                                            <div class="col-sm-8">
                                                <input class="form-control required" type="text" id="firstname" name="firstname" value="{{ old('firstname') }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="lastname" class="col-sm-4 col-form-label">{{ __('Last name') }}</label>
                                            <div class="col-sm-8">
                                                <input class="form-control required" type="text" id="lastname" name="lastname" value="{{ old('lastname') }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="email" class="col-sm-4 col-form-label">{{ __('Email Address') }}</label>
                                            <div class="col-sm-8">
                                                <input class="form-control required" type="email" id="email" name="email"  value="{{ old('email') }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="password" class="col-sm-4 col-form-label">{{ __('Password') }}</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input class="form-control required" type="password" id="password" name="password" required autocomplete="new-password">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-default" type="button" data-toggle="generate">{{ __('Generate') }}</button>
                                                    </div>
                                                </div>
                                                <span class="form-text text-muted" id="generated-password"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card">
                                    <div class="box-body">
                                        <div class="row form-group">
                                            <label for="groups" class="col-sm-4 col-form-label">{{ __('Groups') }}</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="groups" name="groups[]" size="4" multiple>
                                                    @foreach ($groups as $group)
                                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="gender" class="col-sm-4 col-form-label">{{ __('Gender') }}</label>
                                            <div class="col-sm-8">
                                                <select name="gender" id="gender" class="form-control">
                                                    <option value="u">{{ __('Unknown') }}</option>
                                                    <option value="f">{{ __('Female') }}</option>
                                                    <option value="m">{{ __('Male') }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label for="phone_number" class="col-sm-4 col-form-label">{{ __('Phone number') }}</label>
                                            <div class="col-sm-8">
                                                <input class="form-control required" type="tel" id="phone_number" name="phone_number" value="{{ old('phone_number') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <a class="btn btn-danger mr-auto" href="{{ route('customers.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> {{ __('Confirm') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>
        $('[data-toggle="generate"]').generatePassword({
            inputSelector: 'input[name="password"]',
            resultSelector: '#generated-password',
        });
    </script>
@endpush