<form method="POST" action="{{ route('customers.update.company', $customer->id) }}">
    @method('PATCH')
    @csrf
    <input type="hidden" name="company_id" value="{{ $customer->company_id }}">
    <div class="card-body">
        <div class="col-8">
            <div class="row form-group">
                <label for="nip" class="col-sm-3 col-form-label">{{ __('NIP') }}</label>
                <div class="col-sm-7">
                    <div class="input-group">
                        <input class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}" type="text" name="nip" id="nip" value="{{ old('nip') ?: $customer->company->nip }}" >
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button" id="download-gus" data-tip="{{ __('Other company information will be downloaded automatically.') }}">
                                <i class="fa fa-download"></i> {{ __('Download from GUS') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <label for="name" class="col-sm-3 col-form-label">{{ __('Name') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" id="name" name="name" value="{{ old('name') ?: $customer->company->name }}" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="street" class="col-sm-3 col-form-label">{{ __('Street') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}" type="text" id="street" name="street" value="{{ old('street') ?: $customer->company->street }}" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="street_number" class="col-sm-3 col-form-label">{{ __('Street number') }}</label>
                <div class="col-sm-2">
                    <input class="form-control {{ $errors->has('street_number') ? 'is-invalid' : '' }}" type="text" id="street_number" name="street_number"  value="{{ old('street_number') ?: $customer->company->street_number }}" required>
                </div>
                <label for="place_number" class="col-sm-3 col-form-label text-right">{{ __('Place number') }}</label>
                <div class="col-sm-2">
                    <input class="form-control {{ $errors->has('place_number') ? 'is-invalid' : '' }}" type="text" id="place_number" name="place_number"  value="{{ old('place_number') ?: $customer->company->place_number }}">
                </div>
            </div>
            <div class="row form-group">
                <label for="post_code" class="col-sm-3 col-form-label">{{ __('Post code') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('post_code') ? 'is-invalid' : '' }}" type="text" id="post_code" name="post_code" value="{{ old('post_code') ?: $customer->company->post_code }}" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="city" class="col-sm-3 col-form-label">{{ __('City') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" type="text" id="city" name="city" value="{{ old('city') ?: $customer->company->city }}" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="country_code" class="col-sm-3 col-form-label">{{ __('Country') }}</label>
                <div class="col-sm-7">
                    <select class="form-control" id="country_code" name="country_code">
                        @foreach ($countries as $country)
                            <option value="{{ $country->getCode() }}" {{ $country->getCode() === $customer->company->country_code ? 'selected':'' }}>{{ __($country->getName()) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label for="company-phone_number" class="col-sm-3 col-form-label">{{ __('Phone number') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" type="tel" id="company-phone_number" name="phone_number" value="{{ old('phone_number') ?: $customer->company->phone_number }}" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="company-email" class="col-sm-3 col-form-label">{{ __('Email Address') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" id="company-email" name="email"  value="{{ old('email') ?: $customer->company->email }}" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="regon" class="col-sm-3 col-form-label">{{ __('Regon') }}</label>
                <div class="col-sm-7">
                    <input class="form-control {{ $errors->has('regon') ? 'is-invalid' : '' }}" type="text" id="regon" name="regon" value="{{ old('regon') ?: $customer->company->regon }}">
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="text-right">
            <button class="btn btn-primary">
                {{ __('Confirm') }}
            </button>
        </div>
    </div>
</form>