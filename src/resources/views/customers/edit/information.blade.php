<form method="POST" action="{{ route('customers.update', $customer->id) }}">
    @method('PATCH')
    @csrf
    <div class="card-body">
        <div class="row">
            <div class="col-6">
                <div class="form-row form-group">
                    <label for="firstname" class="col-sm-4 col-form-label">{{ __('First name') }}</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control required" id="firstname" name="firstname" value="{{ $customer->firstname }}" required>
                    </div>
                </div>
                <div class="form-row form-group">
                    <label for="lastname" class="col-sm-4 col-form-label">{{ __('Last name') }}</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control required" id="lastname" name="lastname" value="{{ $customer->lastname }}" required>
                    </div>
                </div>
                <div class="form-row form-group">
                    <label for="email" class="col-sm-4 col-form-label">{{ __('Email Address') }}</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control required" id="email" name="email" value="{{ $customer->email }}" required>
                    </div>
                </div>
                <div class="form-row form-group">
                    <label for="phone_number" class="col-sm-4 col-form-label">{{ __('Phone number') }}</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control required" id="phone_number" name="phone_number" value="{{ $customer->phone_number }}" required>
                    </div>
                </div>
                <div class="form-row form-group">
                    <label for="gender" class="col-sm-4 col-form-label">{{ __('Gender') }}</label>
                    <div class="col-sm-4">
                        <select name="gender" id="gender" class="form-control" required>
                            @foreach ($genders as $code => $gender)
                                <option value="{{ $code }}" {{ $customer->gender === $code ? 'selected' : '' }}>{{ __($gender) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-row form-group">
                    <label for="groups" class="col-sm-3 col-form-label">{{ __('Groups') }}</label>
                    <div class="col-sm-6">
                        <select name="groups[]" id="groups" class="form-control" size="5" multiple required>
                            @foreach ($groups as $group)
                                <option value="{{ $group->id }}" {{ (collect(old('groups'))->contains($group->id)) || ($customer->hasGroup($group->id))  ? 'selected':'' }}>{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-row form-group">
                    <label for="password" class="col-sm-3 col-form-label">{{ __('New password') }}</label>
                    <div class="col-sm-6">
                        <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" id="password" name="password" required autocomplete="new-password">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="text-right">
            <button class="btn btn-primary">
                {{ __('Confirm') }}
            </button>
        </div>
    </div>
</form>