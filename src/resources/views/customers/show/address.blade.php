<div class="card-body">
    @foreach($customer->shippingAddress->chunk(3) as $chunkAddress)
        <div class="row {{ $loop->last ? '' : 'mb-4' }}">
            @foreach($chunkAddress as $address)
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-text">{{ $address->fullname }}</div>
                            <div class="card-text">{{ $address->address }}</div>
                            <div class="card-text">{{ "{$address->post_code} {$address->city}" }}</div>
                            <p class="card-text">{{ $address->phone_number }}</p>
                            @if (Route::is('*edit'))
                            <div class="row address-delete-{{ $address->id }} collapse">
                                <div class="col-6">
                                    <form method="post" action="{{ route('account.settings.address-book.destroy', $address->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-link card-link text-success">{{ mb_strtoupper(__('Delete')) }}</button>
                                    </form>
                                </div>
                                <div class="col-6 text-right">
                                    <button class="btn btn-link card-link text-danger" data-toggle="collapse" data-target=".address-delete-{{ $address->id }}" >{{ mb_strtoupper(__('No')) }}</button>
                                </div>
                            </div>
                            <div class="row address-delete-{{ $address->id }} collapse show">
                                <div class="col-6">
                                    <a href="{{ route('account.settings.address-book.edit', $address->id) }}" class="card-link">{{ mb_strtoupper(__('Edit')) }}</a>
                                </div>
                                <div class="col-6 text-right">
                                    <button class="btn btn-link card-link" data-toggle="collapse" data-target=".address-delete-{{ $address->id }}">{{ mb_strtoupper(__('Delete')) }}</button>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
</div>
