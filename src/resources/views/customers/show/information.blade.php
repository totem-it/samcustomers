<div class="col-12">
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('First name') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->firstname }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Last name') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->lastname }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Gender') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ __($customer->gender_name) }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Email Address') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->email }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Phone number') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->phone_number }}</span>
        </div>
    </div>
</div>