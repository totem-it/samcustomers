<div class="col-12">
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('NIP') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->nip }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Name') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->name }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Address') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->address }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Post code') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->post_code }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('City') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->city }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Country') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->country }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Phone number') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->phone_number }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('Email Address') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->email }}</span>
        </div>
    </div>
    <div class="row form-group">
        <span class="col-sm-2 col-form-label">{{ __('REGON') }}</span>
        <div class="col-sm-7">
            <span class="form-control-plaintext">{{ $customer->company->regon }}</span>
        </div>
    </div>
</div>