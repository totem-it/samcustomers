<?php

return [

    /*
   |--------------------------------------------------------------------------
   | Default Admin Authentication guard
   |--------------------------------------------------------------------------
   */
    'guard' => 'customer',

    /*
    |--------------------------------------------------------------------------
    | Default Api Admin Authentication guard
    |--------------------------------------------------------------------------
    */
    'guard-api' => 'customer-api',

    /*
    |--------------------------------------------------------------------------
    | Menu
    |--------------------------------------------------------------------------
    |
    | This is the menu array. It is used by SamAdmin Menu Class to create menu list.
    |
    */
    'menu' => [
        'customers' => [
            'key' => 'customers.view',
            'name' => 'Customer',
            'route' => 'customers',
            'icon-class' => 'fa-users',
            'children' => [
                'customers' => [
                    'key' => 'customers.view',
                    'name' => 'Customers',
                    'route' => 'customers.index',
                    'icon-class' => 'fa-users',
                    'children' => [],
                ],
                'Groups' => [
                    'key' => 'customer-groups.view',
                    'name' => 'Groups',
                    'route' => 'groups.index',
                    'icon-class' => 'fa-archive',
                    'children' => [],
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Country list
    |--------------------------------------------------------------------------
    |
    | This is the country array. It contains country codes.
    |
    */
    'countries' => [
        'pl',
        'gb',
        'de',
    ]
];