<?php

return [

     'customers' => [
         'driver' => 'eloquent',
         'model' => \Totem\SamCustomers\App\Model\Customer::class,
     ],

];
