<?php

return [

    'customer' => [
        'driver' => 'session',
        'provider' => 'customers',
    ],

    'customer-api' => [
        'driver' => 'jwt',
        'provider' => 'customers',
    ],

];
