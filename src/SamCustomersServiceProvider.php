<?php

namespace Totem\SamCustomers;

use Totem\SamAdmin\App\Traits\HasMenu;
use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamCustomersServiceProvider extends ServiceProvider
{
    use TraitServiceProvider, HasMenu;

    public function getNamespace() : string
    {
        return 'sam-customers';
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');
        $this->loadViewsFrom(__DIR__ . '/resources/views', $this->getNamespace());
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->extendMenu(config($this->getNamespace().'.menu'));

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');
        $this->publishes([
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/'.$this->getNamespace()),
        ], $this->getNamespace().'-lang');
        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/'.$this->getNamespace()),
        ], $this->getNamespace().'-views');
        $this->publishes([
            __DIR__ . '/database/migrations' => database_path('migrations'),
        ], $this->getNamespace().'-migrations');

        \Illuminate\Support\Facades\Validator::extendImplicit('current_password', function($attribute, $value, $parameters, $validator) {
            return \Illuminate\Support\Facades\Hash::check($value, auth()->user()->password);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());
        $this->extendAuthGuard(__DIR__ . '/config/auth-guard.php');
        $this->extendAuthProvider(__DIR__ . '/config/auth-provider.php');
        $this->extendAuthPassword(__DIR__ . '/config/auth-password.php');

        $this->setDefaultDriver('customer');

        if ($this->isWebProject()) {
            $this->loadRoutesFrom(__DIR__.'/routes/web.php');
            $this->loadRoutesFrom(__DIR__.'/routes/admin.php');
        }
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->loadRoutesFrom(__DIR__.'/routes/admin-api.php');

        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->registerMiddlewareAlias('customer-api', \Totem\SamAdmin\App\Middleware\JwtMiddleware::class);

        $this->configureBinding([
            \Totem\SamCustomers\App\Repositories\Contracts\CompanyRepositoryInterface::class => \Totem\SamCustomers\App\Repositories\CompanyRepository::class,
            \Totem\SamCustomers\App\Repositories\Contracts\CustomerRepositoryInterface::class => \Totem\SamCustomers\App\Repositories\CustomerRepository::class,
            \Totem\SamCustomers\App\Repositories\Contracts\GroupRepositoryInterface::class => \Totem\SamCustomers\App\Repositories\GroupRepository::class
        ]);
    }

}
