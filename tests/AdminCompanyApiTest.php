<?php

namespace SamCustomers\Tests;

use Totem\SamCustomers\App\Model\Company;
use Totem\SamAdmin\Tests\ApiCrudTest;

class AdminCompanyApiTest extends ApiCrudTest
{

    protected $model = Company::class;
    protected $endpoint = 'customers/companies';

    protected $additionalFields = [
        'code' => 'test',
    ];
    protected $withoutFields = [
        'nip',
    ];

    protected function createModel(array $attributes = []) : Company
    {
        $model = factory($this->model)->create($attributes);
        $model->load('customer');

        return $model;
    }

}
