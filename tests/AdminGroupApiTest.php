<?php

namespace SamCustomers\Tests;

use Totem\SamCustomers\App\Model\Group;
use Totem\SamAdmin\Tests\ApiCrudTest;

class AdminGroupApiTest extends ApiCrudTest
{

    protected $model = Group::class;
    protected $endpoint = 'customers/groups';

    protected $additionalFields = [];
    protected $withoutFields = [
        'code'
    ];

    protected function createModel(array $attributes = []) : Group
    {
        return factory($this->model)->create($attributes);
    }

}