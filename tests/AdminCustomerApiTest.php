<?php

namespace SamCustomers\Tests;

use Totem\SamAddress\App\Model\Address;
use Totem\SamCustomers\App\Model\Company;
use Totem\SamCustomers\App\Model\Customer;
use Totem\SamAdmin\Tests\ApiCrudTest;
use Totem\SamCustomers\App\Model\Group;

class AdminCustomerApiTest extends ApiCrudTest
{

    protected $model = Customer::class;
    protected $endpoint = 'customers';

    protected $additionalFields = [
        'password' => 'secret',
    ];
    protected $withoutFields = [

    ];

    protected function createModel(array $attributes = []) : Customer
    {
        return factory($this->model)->create($attributes);
    }

    public function testEndpointAllAboutCustomer() : void
    {
        $model = $this->createModel();

        $this->call('GET', "/api/{$this->endpoint}/{$model->id}/all")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testEndpointCustomerCompany() : void
    {
        $model = $this->createModel();
        $model->attachCompany(factory(Company::class)->create());

        $this->call('GET', "/api/{$this->endpoint}/{$model->id}/company")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testEndpointCustomerGroups() : void
    {
        $model = $this->createModel();
        $model->attachGroup(factory(Group::class)->create());

        $this->call('GET', "/api/{$this->endpoint}/{$model->id}/groups")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testEndpointCustomerAddresses() : void
    {
        $model = $this->createModel();
        $model->attachAddresses(factory(Address::class, 3)->create());

        $this->call('GET', "/api/{$this->endpoint}/{$model->id}/addresses")->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testGetEverythingAboutCustomer() : void
    {
        $model = $this->createModel();

        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/all");

        $response
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'email',
                    'firstname',
                    'lastname',
                    'gender',
                    'phone_number',
                    'active',
                    'groups' => [],
                    'company' => [],
                    'addresses' => [],
                ]
            ])
            ->assertOk()
        ;
    }

    public function testGetCustomerCompany() : void
    {
        $model = $this->createModel();
        $model->attachCompany(factory(Company::class)->create());

        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/company");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testGetCustomerCompanyFailed() : void
    {
        $model = $this->createModel( ['company_id' => null] );

        $this->json('GET', "/api/{$this->endpoint}/{$model->id}/company")->assertNotFound();
    }

    public function testGetCustomerGroups() : void
    {
        $model = $this->createModel();
        $model->attachGroup(factory(Group::class)->create());

        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/groups");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testGetCustomerGroupsFailed() : void
    {
        $model = $this->createModel();

        $this->json('GET', "/api/{$this->endpoint}/{$model->id}/groups")->assertNotFound();
    }

    public function testGetCustomerAddresses() : void
    {
        $model = $this->createModel();
        $model->attachAddresses(factory(Address::class, 3)->create());

        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/addresses");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testGetCustomerShippingAddress() : void
    {
        $model = $this->createModel();
        $model->attachAddress(factory(Address::class)->state('shipping')->create());

        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/addresses/shipping");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testGetCustomerBillingAddress() : void
    {
        $model = $this->createModel();
        $model->attachAddress(factory(Address::class)->state('billing')->create());

        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/addresses/billing");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testActivateCustomer() : void
    {
        $model = $this->createModel(['active' => 0]);

        $response = $this->json('PATCH', "/api/{$this->endpoint}/{$model->id}", [ 'action' => 'activate' ]);
        $response
            ->assertJson([
                'data' => true
            ])
            ->assertOk()
        ;
    }

    public function testDeactivateCustomer() : void
    {
        $model = $this->createModel(['active' => 1]);

        $response = $this->json('PATCH', "/api/{$this->endpoint}/{$model->id}", [ 'action' => 'deactivate' ]);
        $response
            ->assertJson([
                'data' => true
            ])
            ->assertOk()
        ;
    }

    public function testWrongActionToPatch() : void
    {
        $model = $this->createModel();

        $response = $this->json('PATCH', "/api/{$this->endpoint}/{$model->id}", [ 'action' => 'bla bla bla' ]);
        $response
            ->assertJson([
                'error' => true
            ])
            ->assertStatus(422)
        ;
    }

}