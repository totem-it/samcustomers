<?php

namespace SamCustomers\Tests;

use Totem\SamAddress\App\Model\Address;
use Totem\SamCustomers\App\Model\Company;
use Totem\SamCustomers\App\Model\Customer;
use Totem\SamAdmin\Tests\AttachAdminJwtToken;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomerProfileApiTest extends \Tests\TestCase
{
    use DatabaseTransactions,
        AttachAdminJwtToken;

    protected function setUser() : \Tymon\JWTAuth\Contracts\JWTSubject
    {
        /** @var Customer $user */
        $user =  $this->loginUser ?: factory(Customer::class)->state('no-company')->create();
        $user->attachCompany(factory(Company::class)->create());
        $user->attachAddress(factory(Address::class)->state('billing')->create());
        $user->attachAddresses(factory(Address::class, 3)->state('shipping')->create());
        $this->actingAs($user, $user->getJWTCustomClaims()['gd']);

        return $user;
    }

    public function testGetAuthenticatedCustomerProfileInfo() : void
    {
        $this->json('GET', '/api/profile')
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'email',
                    'firstname',
                    'lastname',
                    'gender',
                    'phone_number',
                    'active',
                    'company',
                    'billingAddress',
                    'shippingAddress',
                ]
            ])
            ->assertOk();
    }
}
