<?php

namespace SamCustomers\Tests;

use Totem\SamAcl\App\Model\Role;
use Totem\SamCustomers\App\Model\Customer;
use Totem\SamCustomers\Tests\AttachCustomerJwtToken;
use Totem\SamAdmin\Tests\ApiCrudTest;

class CompanyApiTest extends ApiCrudTest
{

    use AttachCustomerJwtToken;

    protected $endpoint = 'customers';
    protected $model = Customer::class;
    protected $additionalFields = [
        'password' => 'secret',
        'roles' => [ 1 ]
    ];

    protected function createModel(array $attributes = []) : Customer
    {
        $this->loginAs($this->setUser());

        $model = factory($this->model)->create($attributes);
//        $model->attachPermissions(Role::all());
//        $model->load('roles');

        return $model;
    }

//    public function testShowUserRoles() : void
//    {
//        $model = $this->createModel();
//
//        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/roles");
//        $response
//            ->assertJson([
//                'data' => []
//            ])
//            ->assertOk()
//        ;
//    }
//
//    public function testShowUserPermissions() : void
//    {
//        $model = $this->createModel();
//
//        $response = $this->json('GET', "/api/{$this->endpoint}/{$model->id}/permissions");
//        $response
//            ->assertJson([
//                'data' => true
//            ])
//            ->assertOk()
//        ;
//    }
//
//    public function testActivateUser() : void
//    {
//        $model = $this->createModel(['active' => 0]);
//
//        $response = $this->json('PATCH', "/api/{$this->endpoint}/{$model->id}", [ 'action' => 'activate' ]);
//        $response
//            ->assertJson([
//                'data' => true
//            ])
//            ->assertOk()
//        ;
//    }
//
//    public function testDeactivateUser() : void
//    {
//        $model = $this->createModel(['active' => 1]);
//
//        $response = $this->json('PATCH', "/api/{$this->endpoint}/{$model->id}", [ 'action' => 'deactivate' ]);
//        $response
//            ->assertJson([
//                'data' => true
//            ])
//            ->assertOk()
//        ;
//    }
//
//
//    public function testWrongActionToPatch() : void
//    {
//        $model = $this->createModel();
//
//        $response = $this->json('PATCH', "/api/{$this->endpoint}/{$model->id}", [ 'action' => 'bla bla bla' ]);
//        $response
//            ->assertJson([
//                'error' => true
//            ])
//            ->assertStatus(422)
//        ;
//    }

}
