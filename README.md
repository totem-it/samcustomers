Sam Customer Component
================

This package manage customers in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/files/totem.png)

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~5.6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-admin ~1.0.*](https://bitbucket.org/totem-it/samadmin)  


Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require totem-it/sam-customers
```

Replace default passwords in `config/auth.php` file.

```php
'defaults' => [
    'guard' => 'web',
    'passwords' => 'customers',
],
```

Replace default web guard in `config/auth.php` file.

```php
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'customers',
    ],
    ...
],
```

Create new provider in `config/auth.php` file.

```php
'providers' => [
    ...
    'customers' => [
        'driver' => 'eloquent',
        'model' => \Totem\SamCustomers\App\Model\Customer::class,
    ],
],
```

Create new reset passwords in `config/auth.php` file.

```php
'passwords' => [
    'customers' => [
        'provider' => 'customers',
        'table' => 'password_resets',
        'expire' => 60,
    ],
],
```

Usage
-------------


Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)